<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $station_code = clean($conn,$_POST['station_code']);
  $station_name = clean($conn,$_POST['station_name']);

  if (!isset($_GET['stationid'])) {

    if ($station_code == "" || $station_name == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif(!preg_match("/^[0-9]+$/", $station_code)){
      
      setNotification(2,$lang['station_code_error']);
      
    }else{
    
      $sql = "INSERT INTO stations ".
      "(station_code, station_name) ".
      "VALUES ".
      "('$station_code','$station_name')";
      $retval = mysqli_query($conn,$sql);
      mysqliDie($retval);  
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The station with name "'.$station_name.'" and code "'.$station_code.'" was added.');
          
      setNotification(1,$lang['save_success']);

    }

  }elseif(isset($_GET['stationid'])){

    if ($station_code == "" || $station_name == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif(!preg_match("/^[0-9]+$/", $station_code)){
      
      setNotification(2,$lang['station_code_error']);
      
    }else{
    
      $sql = "UPDATE stations SET station_code='".$station_code."',station_name='".$station_name."' WHERE station_id='".$_GET['stationid']."'";
      $update = mysqli_query($conn,$sql);
      mysqliDie($update);
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The station with name "'.$station_name.'" and code "'.$station_code.'" was updated.');
          
      setNotification(1,$lang['update_success']);

    }
  }
}
?>