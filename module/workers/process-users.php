<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM users ORDER BY user_id";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		($row['login_date'] == 0) ? $loginDate = 'Pending' : $loginDate = date('j-m-Y',$row['login_date']);
		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.ucwords(strtolower($row['firstname'].' '.$row['lastname'])).'",
		      "'.strtolower($row['username']).'",
		      "'.userRole($row['userlevel']).'",
		      "'.$loginDate.'",
		      "'.date('j-m-Y',$row['join_date']).'",
		      "<a href=\"?page=management&subpage=setup+users&useraction=edit&userid='.$row['user_id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-pencil\"></i> Edit</a> <a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['user_id'].'\" onclick=\"confirmDelete('. "'del" . $row['user_id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-users.php?userid=" . $row['user_id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>