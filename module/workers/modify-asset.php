<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {

  //form variables
  $tag_no = clean($conn,$_POST['tag_no']);
  $item_name = clean($conn,$_POST['item_name']);
  $quantity = clean($conn,$_POST['quantity']);
  $serial_no = clean($conn,$_POST['serial_no']);
  $cost = clean($conn,$_POST['cost']);
  $category = clean($conn,$_POST['category']);
  $date = clean($conn,$_POST['date']);
  $condition = clean($conn,$_POST['condition']);
  $description = clean($conn,$_POST['description']);
  $time = time();

  //default description
  ($description == "") ? $description =  "Asset" : $description = $description;

  if (!isset($_GET['assetid'])) {   

    if ($tag_no == "" || $item_name == "" || $quantity == "" || $date == "" || $category == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif($cost <= 0){
    
      setNotification(2,$lang['no_amount_error']);
    
    }else{
    
      $sql = "INSERT INTO asset_register ".
      "(tag_no, item_name, description, quantity, serial_no, category, cost, date, status, time) ".
      "VALUES ".
      "('$tag_no','$item_name','$description','$quantity','$serial_no','$category','$cost','$date','$condition','$time')";
      $retval = mysqli_query($conn,$sql);
      mysqliDie($retval);  
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The asset with name "'.$item_name.'" was added to the register.');
          
      setNotification(1,$lang['save_success']);  

    }

  }elseif (isset($_GET['assetid'])){

    if ($tag_no == "" || $item_name == "" || $quantity == "" || $date == "" || $category == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif($cost <= 0){
    
      setNotification(2,$lang['no_amount_error']);
    
    }else{
    
      $sql = "UPDATE asset_register SET tag_no='".$tag_no."',item_name='".$item_name."',description='".$description."',quantity='".$quantity."',serial_no='".$serial_no."',category='".$category."',cost='".$cost."',date='".$date."',status='".$condition."' WHERE id='".$_GET['assetid']."'";
      $update = mysqli_query($conn,$sql);
      mysqliDie($update);
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The asset with name "'.$item_name.'" was updated.');
          
      setNotification(1,$lang['update_success']);

    }
  }
}
?>