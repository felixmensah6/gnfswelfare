<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $firstname = clean($conn,$_POST['firstname']);
  $lastname = clean($conn,$_POST['lastname']);
  $username = clean($conn,$_POST['username']);
  $password1 = clean($conn,$_POST['password1']);
  $password2 = clean($conn,$_POST['password2']);
  $userlevel = clean($conn,$_POST['userlevel']);
  $permissions = $_POST['perm'];
  $joindate = time();

  if (!isset($_GET['userid'])) {

    if ($firstname == "" || $lastname == "" || $username == "" || $password1 == "" || $userlevel == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif(!preg_match("/^[A-Za-z0-9]+$/", $username)){
      
      setNotification(2,$lang['username_invalid_error']);
      
    }elseif(strlen($username) < 5){
      
      setNotification(2,$lang['username_length_error'],'5 or more characters long.');
      
    }elseif(strlen($password1) < 8 || strlen($password1) > 30){
      
      setNotification(2,$lang['password_length_error'],'8 or more characters long.');
      
    }elseif($password1 != $password2){
      
      setNotification(2,$lang['password_match_error']);
      
    }elseif(empty($permissions)){
      
      setNotification(2,$lang['no_permission_error']);
      
    }else{
    
      //check to make sure the record exist or not
      $qry="SELECT * FROM users WHERE username='".$username."'";
      $checkresult = mysqli_query($conn,$qry);
      $chrow = mysqli_fetch_array($checkresult);

      //seperate values with commas
      $perms = implode(',',$permissions);

      if(mysqli_num_rows($checkresult) == 1) {
        
        setNotification(2,$lang['user_exist_error']);
    
      }else{
        $sql = "INSERT INTO users ".
        "(username, password, firstname, lastname, userlevel, permissions, join_date) ".
        "VALUES ".
        "('$username','".sha1($password1)."','$firstname','$lastname','$userlevel','$perms','$joindate')";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);  
        
        //insert history
        $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
        addLog($conn,$logger,'A new user "'.$firstname.' '.$lastname.'" was added to the Database.');
            
        setNotification(1,$lang['save_success']);

      }
    }
  }elseif(isset($_GET['userid'])){

    if ($firstname == "" || $lastname == "" || $userlevel == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif(empty($permissions)){
      
      setNotification(2,$lang['no_permission_error']);
      
    }else{
    
      //check to make sure the record exist or not
      $qry="SELECT * FROM users WHERE username='".$username."'";
      $checkresult = mysqli_query($conn,$qry);
      $chrow = mysqli_fetch_array($checkresult);

      //seperate values with commas
      $perms = implode(',',$permissions);

      if(mysqli_num_rows($checkresult) == 1) {
        
        setNotification(2,$lang['user_exist_error']);
    
      }else{
        $sql = "UPDATE users SET firstname='".$firstname."',lastname='".$lastname."',userlevel='".$userlevel."',permissions='".$perms."' WHERE user_id=".$_GET['userid']."";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);  
        
        //insert history
        $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
        addLog($conn,$logger,'The user "'.$firstname.' '.$lastname.'" was updated.');
            
        setNotification(1,$lang['update_success']);

      }
    }

  }
}
?>