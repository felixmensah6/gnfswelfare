<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM over_deduction_details WHERE member_id=".$_GET['memberid']."";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['description'].'",
		      "'.getPeriod($row['period']).'",
		      "'.number_format($row['debit'],2).'",
		      "'.number_format($row['credit'],2).'",
		      "'.$row['date'].'",
		      "'.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['id'].'\" onclick=\"confirmDelete('. "'del" . $row['id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-over-deduction-details.php?detailid=" . $row['id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>