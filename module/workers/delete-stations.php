<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['stationid'])){
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM stations WHERE station_id="'.$_GET['stationid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'The station "'.itemInfo($conn,'stations','station_id',$_GET['stationid'],'station_name').'" was deleted from the system.');
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);

}
?>