<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $firstname = clean($conn,$_POST['firstname']);
  $lastname = clean($conn,$_POST['lastname']);
  $staff_id = clean($conn,$_POST['staff_id']);
  $station = clean($conn,$_POST['station']);
  $email = clean($conn,$_POST['email']);
  $birthday = clean($conn,$_POST['birthday']);
  $gender = clean($conn,$_POST['gender']);
  $marital_status = clean($conn,$_POST['marital_status']);
  $spouse = clean($conn,$_POST['spouse']);
  $children = clean($conn,$_POST['children']);
  $contact = clean($conn,$_POST['contact']);
  $father = clean($conn,$_POST['father']);
  $mother = clean($conn,$_POST['mother']);
  $address = clean($conn,$_POST['address']);
  $joindate = time();
  
  if(!empty($_FILES['userfile']['name'])){
    $filename = $_FILES['userfile']['name'];
    //calculate dimension
    list($width, $height) = getimagesize($_FILES['userfile']['tmp_name']); 
  }else{
    $filename = '';
  }
  
  $filesize = $_FILES['userfile']['size'];
  $allowed_filetypes = array('.jpg','.gif','.png'); 
  $max_filesize = 5242880;  //5mb
  $upload_path = '../content/images/tmp/';
  $ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); 
  $ext = strtolower($ext); 

  if ($firstname == "" || $lastname == "" || $staff_id == "" || $station == "") {
    
    setNotification(2,'',$lang['blank_input_error']);
        
  }elseif($marital_status == 2 && $spouse == ""){
    
    setNotification(2,'',$lang['spouse_name_error']);
    
  }elseif($email != "" && !preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
    
    setNotification(2,'',$lang['email_error']);
    
  }elseif($filename != '' && $filesize > $max_filesize){
    
    setNotification(2,'',$lang['filesize_error'],($max_filesize / 1048576).'mb.');
    
  }elseif($filename != '' && ($width < 500 || $height < 500)){
    
   setNotification(2,'',$lang['image_dimension_error']);
    
  }elseif($filename != '' && !is_writable($upload_path)){
    
    setNotification(2,'',$lang['folder_write_error']);
    
  }elseif($filename != '' && !in_array($ext,$allowed_filetypes)){
    
    setNotification(2,'',$lang['image_ext_error']);
    
  }else{
          
    if($filename != ''){
    
        // Upload the file to your specified path.
        $new_file_name = md5($staff_id).$ext;
        move_uploaded_file($_FILES['userfile']['tmp_name'],$upload_path . $new_file_name);
        
        // *** Include the class
        include("../class/image-resize.class.php");
        
        // *** 1) Initialise / load image
        $resizeObj = new resize($upload_path . $new_file_name);
      
        // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
        $resizeObj -> resizeImage(500,500,'crop');
      
        // *** 3) Save image
        $resizeObj -> saveImage('../content/images/members/'.$new_file_name, 100);
        
        // *** 7) Delete original image & existing image
        unlink($upload_path . $new_file_name);

    }
    
    //update database with new data if everything is OK!
    ($filename == "") ?  $new_file_name = "" : $new_file_name = ",photo='".$new_file_name."'";
    
    $sql = "UPDATE members SET staff_id='".$staff_id."',station='".$station."',firstname='".$firstname."',lastname='".$lastname."',birthday='".$birthday."',gender='".$gender."',marital_status='".$marital_status."',spouse_name='".$spouse."',children='".$children."',contact='".$contact."',father_name='".$father."',mother_name='".$mother."',address='".$address."',email='".$email."' ".$new_file_name." WHERE member_id='".$_GET['memberid']."'";
    $update = mysqli_query($conn,$sql);
    mysqliDie($update);
    
    //insert history
    $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
    addLog($conn,$logger,'The member information for "'.$firstname.' '.$lastname.'" was updated.');
        
    setNotification(1,'',$lang['update_success']); 

  }
}
?>