<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['expid'])){
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM expenditure WHERE exp_id="'.$_GET['expid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,itemInfo($conn,'expense_type','id',itemInfo($conn,'expenditure','exp_id',$_GET['expid'],'exp_name'),'expense').'" of GHC '.number_format(itemInfo($conn,'expenditure','exp_id',$_GET['expid'],'exp_amount'),2).' was deleted.');
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);
}
?>