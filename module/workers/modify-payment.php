<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

if (isset($_POST['save'])) {

	//form variables
	$member_id = clean($conn,$_POST['member_id']);
	$staff_id = itemInfo($conn,'members','member_id',$member_id,'staff_id');
	$fullname = ucwords(strtolower(itemInfo($conn,'members','member_id',$member_id,'firstname').' '.itemInfo($conn,'members','member_id',$member_id,'lastname')));
	$period = clean($conn,$_POST['date']);
	$payment_type = clean($conn,$_POST['payment_type']);
	$payment_name = "Loan Recovery";
	$account = clean($conn,$_POST['account']);
	$loan_id = clean($conn,$_SESSION['loan_id']);
	$loan_name = clean($conn,$_POST['loan_name']);
	$duration_left = (itemInfo($conn,'loan_account','loan_id',$loan_id,'duration_left') >= 1) ? (itemInfo($conn,'loan_account','loan_id',$loan_id,'duration_left') - 1) : "";
	$amount = clean($conn,$_POST['amount']);
	$description = clean($conn,$_POST['payment_desc']);
	$period_time = strtotime(str_replace('/', '-', $period));
	$period_day = date('d',$period_time);
	$period_month = date('m',$period_time);
	$period_year = date('Y',$period_time);
	$date = date('d/m/Y',time());
	$time = time();

	if($payment_type == 1){
		$credit = $amount;
		$debit = 0;
	}elseif($payment_type == 2){
		$credit = 0;
		$debit = $amount;
	}

	//Set default description
	($description == "") ? $description = 'Payment' : $description = $description;


	if ($member_id == "" || $payment_type == "" || $account == "" || $amount == "" || $period == "") {

		setNotification(2,$lang['blank_input_error']);

	}elseif(!is_numeric($amount)){

		setNotification(2,$lang['is_numeric_error']);

	}elseif($amount <= 0){

		setNotification(2,$lang['no_amount_error']);

	}else{


		/********************************************
		* WELFARE ACCOUNT
		********************************************/
		if($account == 1){

		    //CREDIT
		    if($payment_type == 1) {

		      //if results exist
		      $qry = "SELECT * FROM welfare_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) == 1) {

		        $sql = "UPDATE welfare_account SET months=months + 0,credit=credit + ".$amount.",debit=debit + 0 WHERE member_id=".$member_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO welfare_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        $sql = "INSERT INTO welfare_account ".
		        "(member_id, staff_id, account_holder, months, credit, debit, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$fullname','0','$credit','$debit','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO welfare_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }

		    //DEBIT
		    }elseif($payment_type == 2) {

		      //if results exist
		      $qry = "SELECT * FROM welfare_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) == 1) {

		        $sql = "UPDATE welfare_account SET months=months,credit=credit + 0,debit=debit + ".$amount." WHERE member_id=".$member_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO welfare_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        setNotification(2,$lang['no_credit_error']);

		      }
		  	}

		/********************************************
		* FUNERAL & SOCIAL ACCOUNT
		********************************************/
		}elseif($account == 2){

		    //CREDIT
		    if($payment_type == 1) {

		      //if results exist
		      $qry = "SELECT * FROM social_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) == 1) {

		        $sql = "UPDATE social_account SET months=months + 0,credit=credit + ".$amount.",debit=debit + 0 WHERE member_id=".$member_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO social_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        $sql = "INSERT INTO social_account ".
		        "(member_id, staff_id, account_holder, months, credit, debit, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$fullname','0','$credit','$debit','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO social_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }

		    //DEBIT
		    }elseif($payment_type == 2) {

		      //if results exist
		      $qry = "SELECT * FROM social_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) == 1) {

		        $sql = "UPDATE social_account SET months=months,credit=credit + 0,debit=debit + ".$amount." WHERE member_id=".$member_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO social_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        setNotification(2,$lang['no_credit_error']);

		      }

		    }

		/********************************************
		* LOAN ACCOUNT
		********************************************/
		}elseif($account == 3){

		    //CREDIT
		    if($payment_type == 1) {

		      //if results exist
		      $qry = "SELECT * FROM loan_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) >= 1) {

		        $sql = "UPDATE loan_account SET duration_left=".$duration_left.", credit=credit + ".$amount." WHERE loan_id=".$loan_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO loan_recovery ".
		        "(loan_id, member_id, payment_name, period, duration_left, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$loan_id','$member_id','$payment_name','$period','$duration_left','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        setNotification(2,$lang['no_loan_error']);

		      }

		    //DEBIT
		    }elseif($payment_type == 2) {

		      setNotification(2,$lang['grant_loan_error']);

		  	}

		/********************************************
		* OVER DEDUCTIONS ACCOUNT
		********************************************/
		}elseif($account == 4){

		    //CREDIT
		    if($payment_type == 1) {

		      //if results exist
		      $qry = "SELECT * FROM over_deduction_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) == 1) {

		        $sql = "UPDATE over_deduction_account SET months=months + 1,credit=credit + ".$amount.",debit=debit + 0,date='$date',time='$time' WHERE member_id=".$member_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO over_deduction_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        $sql = "INSERT INTO over_deduction_account ".
		        "(member_id, staff_id, account_holder, months, credit, debit, date, status, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$fullname','1','$credit','$debit','$date',1,'$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO over_deduction_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }

		    //DEBIT
		    }elseif($payment_type == 2) {

		      //if results exist
		      $qry = "SELECT * FROM over_deduction_account WHERE member_id='".$member_id."'";
		      $result = mysqli_query($conn,$qry);

		      if(mysqli_num_rows($result) == 1) {

		        $sql = "UPDATE over_deduction_account SET months=months,credit=credit + 0,debit=debit + ".$amount.",date='$date',time='$time' WHERE member_id=".$member_id."";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        $sql = "INSERT INTO over_deduction_details ".
		        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
		        "VALUES ".
		        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
		        $retval = mysqli_query($conn,$sql);
		        mysqliDie($retval);

		        setNotification(1,$lang['save_success']);

		      }else{

		        setNotification(2,$lang['no_credit_error']);

		      }

		    }

		}

	}
}
?>
