<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {

  //form variables
  $cat_name = clean($conn,$_POST['cat_name']);

  if (!isset($_GET['assetcategoryid'])) {   

    if ($cat_name == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }else{
    
      $sql = "INSERT INTO asset_category ".
      "(category) ".
      "VALUES ".
      "('$cat_name')";
      $retval = mysqli_query($conn,$sql);
      mysqliDie($retval);  
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The asset category with name "'.$cat_name.'" was added.');
          
      setNotification(1,$lang['save_success']);  

    }

  }elseif (isset($_GET['assetcategoryid'])){

    if ($cat_name == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }else{
    
      $sql = "UPDATE asset_category SET category='".$cat_name."' WHERE id='".$_GET['assetcategoryid']."'";
      $update = mysqli_query($conn,$sql);
      mysqliDie($update);
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The asset category with name "'.$cat_name.'" was updated.');
          
      setNotification(1,$lang['update_success']);

    }
  }
}
?>