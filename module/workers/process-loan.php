<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM loan_account";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['staff_id'].'",
		      "'.userTitle(itemInfo($conn,'members','member_id',$row['member_id'],'gender')).' '.ucwords(strtolower(itemInfo($conn,'members','member_id',$row['member_id'],'firstname').' '.itemInfo($conn,'members','member_id',$row['member_id'],'lastname'))).'",
		      "'.$row['payment_name'].'",
		      "'.loanStatus($row['credit'] - $row['debit']).'",
		      "'.paymentDuration($row['duration']).'",
		      "<a href=\"?page=members&subpage=manage+members&useraction=details&section=loans&memberid='.$row['member_id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-search\"></i> Details</a>"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>