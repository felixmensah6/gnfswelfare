<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database 
if(isset($_GET['memberid'])){

	$del_user = itemInfo($conn,'members','member_id',$_GET['memberid'],'firstname').' '.itemInfo($conn,'members','member_id',$_GET['memberid'],'lastname');
	
	//delete profile photo
	if(itemInfo($conn,'members','member_id',$_GET['memberid'],'photo') != NULL){
	  unlink('../content/images/members/'.itemInfo($conn,'members','member_id',$_GET['memberid'],'photo'));
	}

	//delete from table name and row identifier
	$sql = 'DELETE FROM members WHERE member_id="'.$_GET['memberid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'The member "'.$del_user.'" was deleted from the system.');
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);
}
?>