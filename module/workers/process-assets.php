<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM asset_register";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['tag_no'].'",
		      "'.ucwords(strtolower($row['item_name'])).'",
		      "'.$row['quantity'].'",
		      "'.number_format($row['cost'],2).'",
		      "'.ucwords(strtolower(itemInfo($conn,'asset_category','id',$row['category'],'category'))).'",
		      "'.assetStatus($row['status']).'",
		      "'.((checkAccess('2',$_SESSION['user_perms'])) ? '<a href=\"?page=management&subpage=asset+register&useraction=edit&assetid='.$row['id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-pencil\"></i> Edit</a>' : '' ).' '.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['id'].'\" onclick=\"confirmDelete('. "'del" . $row['id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-esset.php?assetid=" . $row['id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>