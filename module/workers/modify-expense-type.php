<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {

  //form variables
  $expense = clean($conn,$_POST['expense']);

  if (!isset($_GET['expensetypeid'])) {   

    if ($expense == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }else{
    
      $sql = "INSERT INTO expense_type ".
      "(expense) ".
      "VALUES ".
      "('$expense')";
      $retval = mysqli_query($conn,$sql);
      mysqliDie($retval);  
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The expense type with name "'.$expense.'" was added.');
          
      setNotification(1,$lang['save_success']);

    }

  }elseif (isset($_GET['expensetypeid'])){

    if ($expense == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }else{
    
      $sql = "UPDATE expense_type SET expense='".$expense."' WHERE id='".$_GET['expensetypeid']."'";
      $update = mysqli_query($conn,$sql);
      mysqliDie($update);
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'The expense type with name "'.$expense.'" was updated.');
          
      setNotification(1,$lang['update_success']);

    }
  }
}
?>