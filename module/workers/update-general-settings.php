<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $company_name = clean($conn,$_POST['company_name']);
  $interest = clean($conn,$_POST['interest']);
  $welfare_dues = clean($conn,$_POST['welfare_dues']);
  $funeral_social = clean($conn,$_POST['funeral_social']);
  $auto_logout = clean($conn,$_POST['auto_logout']);
  $controller_charge = clean($conn,$_POST['controller_charge']);
  $currency = clean($conn,$_POST['currency']);

  if ($company_name == "" || $interest == "" || $welfare_dues == "" || $funeral_social == "" || $controller_charge == "") {
    
    setNotification(2,$lang['blank_input_error']);
        
  }elseif(!is_numeric($welfare_dues) || !is_numeric($funeral_social)){
    
    setNotification(2,$lang['is_numeric_error']);
    
  }elseif(!is_numeric($interest)){
    
    setNotification(2,$lang['interest_rate_error']);
    
  }else{
    
    $settings = array(
           1 => $company_name,
           2 => $auto_logout,
           3 => $interest,
           4 => $welfare_dues,
           5 => $funeral_social,
           6 => $controller_charge,
           7 => $currency
          );

    foreach($settings as $key => $value){ 
    
      //update values
      itemUpdate($conn,'system_settings','id',$key,'value',$value);
      
    }
        
    setNotification(1,$lang['update_success']); 

  }
}
?>