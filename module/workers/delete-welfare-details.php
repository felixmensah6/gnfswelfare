<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['detailid'])){

	$period = getPeriod(itemInfo($conn,'welfare_details','id',$_GET['detailid'],'period'));
	$staffid = itemInfo($conn,'welfare_details','id',$_GET['detailid'],'staff_id');
	$credit = itemInfo($conn,'welfare_details','id',$_GET['detailid'],'credit');
	$debit = itemInfo($conn,'welfare_details','id',$_GET['detailid'],'debit');
	$months = ($credit > 0) ? 1 : 0;

	//run update first
	$sql = 'UPDATE welfare_account SET months=months-'.$months.',credit=credit-'.$credit.',debit=debit-'.$debit.' WHERE staff_id='.$staffid.'';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM welfare_details WHERE id="'.$_GET['detailid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'The user deleted '.$period.' welfare payment for member with Staff ID '.$staffid);
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);

}
?>