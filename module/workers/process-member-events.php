<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM social_event WHERE member_id=".$_GET['memberid']."";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['event_name'].'",
		      "'.number_format($row['amount_given'],2).'",
		      "'.$row['event_desc'].'",
		      "'.$row['event_date'].'",
		      "'.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['event_id'].'\" onclick=\"confirmDelete('. "'del" . $row['event_id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-member-event.php?eventid=" . $row['event_id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>