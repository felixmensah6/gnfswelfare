<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

if(isset($_POST['payall'])){

	//form variables
	$password = clean($conn,$_POST['password']);
	$amount = clean($conn,$_POST['amount']);
	$period = clean($conn,$_POST['period']);
	$unix_date = strtotime(str_replace('/', '-', $period));
	$description = 'Monthly Payment';
	$payment_period = date('F Y',$unix_date);
	$period_day = date('d',$unix_date);
	$period_month = date('m',$unix_date);
	$period_year = date('Y',$unix_date);
	$date = date('d/m/Y',time());
	$time = time();

	
	//WELFARE ACCOUNT
	if($_POST['account'] == 1){

		if($password == "" || $period == "") {
		  
		  echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button>
		    	<strong>Error!</strong> '.$lang['blank_input_error'].'</div>';
				  
		}elseif(sha1($password) != itemInfo($conn,'users','user_id',UserID(),'password')){

		  echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button>
		    	<strong>Error!</strong> '.$lang['wrong_password_error'].'</div>';

		}elseif(itemInfo($conn,'users','user_id',UserID(),'userlevel') != 1){

		  echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button>
		    	<strong>Error!</strong> '.$lang['wrong_perm_error'].'</div>';

		}else{

			//select all from table
			$sel_sql = "SELECT * FROM members WHERE status=1";
			$sel_res = mysqli_query($conn,$sel_sql);
			mysqliDie($sel_res);
			$count_rows = mysqli_num_rows($sel_res);
			$aff_rows = 0;
			while ($row = mysqli_fetch_assoc($sel_res)) {
				
				$member_id = $row['member_id'];
				$staff_id = $row['staff_id'];
				$fullname = ucwords(strtolower($row['firstname'].' '.$row['lastname']));

				$sel2_sql = "SELECT * FROM welfare_account WHERE member_id = ".$member_id."";
				$sel2_res = mysqli_query($conn,$sel2_sql);
				mysqliDie($sel2_res);
				
				if(mysqli_num_rows($sel2_res) == 0){

					//Insert into welfare account table
					$ins_sql = "INSERT INTO welfare_account ".
					"(member_id, staff_id, account_holder, months, credit, debit, date, status, time) ".
					"VALUES ".
					"('$member_id','$staff_id','$fullname','1','$amount','0','$date','1','$time')";
					$ins_res = mysqli_query($conn,$ins_sql);
					mysqliDie($ins_res);

					//Insert into welfare account details
					$ins2_sql = "INSERT INTO welfare_details ".
					"(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
					"VALUES ".
					"('$member_id','$staff_id','$period','$amount','0','$description','$period_day','$period_month','$period_year','$date','$time')";
					$ins2_res = mysqli_query($conn,$ins2_sql);
					mysqliDie($ins2_res);

				}else{

			        //Update if record already exist
			        $sql = "UPDATE welfare_account SET months=months + 1,credit=credit + ".$amount.",debit=debit + 0,date=".$date.",status=1,time=".$time." WHERE member_id=".$member_id."";
			        $retval = mysqli_query($conn,$sql);
			        mysqliDie($retval); 

					//Insert into welfare account details
					$ins3_sql = "INSERT INTO welfare_details ".
					"(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
					"VALUES ".
					"('$member_id','$staff_id','$period','$amount','0','$description','$period_day','$period_month','$period_year','$date','$time')";
					$ins3_res = mysqli_query($conn,$ins3_sql);
					mysqliDie($ins3_res); 

				}
				
				$aff_rows++; 
				
			}	

			echo '<div class="alert alert-success" role="alert"><button class="close" data-dismiss="alert">×</button>
			<strong>Success!</strong> '.$aff_rows.' out of '.$count_rows.' Members have been paid for '.$payment_period.'.</div>';		

		}

	}

	
	//FUNERAL & SOACIAL ACCOUNT
	if($_POST['account'] == 2){

		if($password == "" || $period == "") {
		  
		  echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button>
		    	<strong>Error!</strong> '.$lang['blank_input_error'].'</div>';
				  
		}elseif(sha1($password) != itemInfo($conn,'users','user_id',UserID(),'password')){

		  echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button>
		    	<strong>Error!</strong> '.$lang['wrong_password_error'].'</div>';

		}elseif(itemInfo($conn,'users','user_id',UserID(),'userlevel') != 1){

		  echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button>
		    	<strong>Error!</strong> '.$lang['wrong_perm_error'].'</div>';

		}else{

			//select all from table
			$sel_sql = "SELECT * FROM members WHERE status=1";
			$sel_res = mysqli_query($conn,$sel_sql);
			mysqliDie($sel_res);
			$count_rows = mysqli_num_rows($sel_res);
			$aff_rows = 0;
			while ($row = mysqli_fetch_assoc($sel_res)) {
				
				$member_id = $row['member_id'];
				$staff_id = $row['staff_id'];
				$fullname = ucwords(strtolower($row['firstname'].' '.$row['lastname']));

				$sel2_sql = "SELECT * FROM social_account WHERE member_id = ".$member_id."";
				$sel2_res = mysqli_query($conn,$sel2_sql);
				mysqliDie($sel2_res);
				
				if(mysqli_num_rows($sel2_res) == 0){

					//Insert into welfare account table
					$ins_sql = "INSERT INTO social_account ".
					"(member_id, staff_id, account_holder, months, credit, debit, date, status, time) ".
					"VALUES ".
					"('$member_id','$staff_id','$fullname','1','$amount','0','$date','1','$time')";
					$ins_res = mysqli_query($conn,$ins_sql);
					mysqliDie($ins_res);

					//Insert into welfare account details
					$ins2_sql = "INSERT INTO social_details ".
					"(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
					"VALUES ".
					"('$member_id','$staff_id','$period','$amount','0','$description','$period_day','$period_month','$period_year','$date','$time')";
					$ins2_res = mysqli_query($conn,$ins2_sql);
					mysqliDie($ins2_res);

				}else{

			        //Update if record already exist
			        $sql = "UPDATE social_account SET months=months + 1,credit=credit + ".$amount.",debit=debit + 0,date=".$date.",status=1,time=".$time." WHERE member_id=".$member_id."";
			        $retval = mysqli_query($conn,$sql);
			        mysqliDie($retval); 

					//Insert into welfare account details
					$ins3_sql = "INSERT INTO social_details ".
					"(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
					"VALUES ".
					"('$member_id','$staff_id','$period','$amount','0','$description','$period_day','$period_month','$period_year','$date','$time')";
					$ins3_res = mysqli_query($conn,$ins3_sql);
					mysqliDie($ins3_res); 

				}
				
				$aff_rows++; 
				
			}	

			echo '<div class="alert alert-success" role="alert"><button class="close" data-dismiss="alert">×</button>
			<strong>Success!</strong> '.$aff_rows.' out of '.$count_rows.' Members have been paid for '.$payment_period.'.</div>';

		}

	}


}

?>