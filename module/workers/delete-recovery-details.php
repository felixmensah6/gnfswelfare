<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['recoveryid'])){

	$staffid = itemInfo($conn,'loan_account','loan_id',itemInfo($conn,'loan_recovery','recovery_id',$_GET['recoveryid'],'loan_id'),'staff_id');
	$credit = itemInfo($conn,'loan_recovery','recovery_id',$_GET['recoveryid'],'credit');
	$debit = itemInfo($conn,'loan_recovery','recovery_id',$_GET['recoveryid'],'debit');
	$loanid = itemInfo($conn,'loan_recovery','recovery_id',$_GET['recoveryid'],'loan_id');

	//run update first
	$sql = 'UPDATE loan_account SET duration_left=duration_left+1,credit=credit-'.$credit.',debit=debit-'.$debit.' WHERE loan_id='.$loanid.'';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM loan_recovery WHERE recovery_id="'.$_GET['recoveryid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'A recovery payment made by Staff ID '.$staffid.' was deleted.');
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);

}
?>