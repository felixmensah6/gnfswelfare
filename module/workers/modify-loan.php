<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $member_id = clean($conn,$_POST['member_id']);
  $staff_id = itemInfo($conn,'members','member_id',$member_id,'staff_id');
  $rate = clean($conn,$_POST['rate']);
  $per_rate = ($rate / 100);
  $loan_amount = clean($conn,$_POST['loan_amount']);
  $paymentname = 'Loan - ('.number_format($loan_amount,2).' + '.$rate.'%)';
  $duration = (int) clean($conn,$_POST['duration']);
  $duration_type = clean($conn,$_POST['duration_type']);
  $loan_date = clean($conn,$_POST['loan_date']);
  $loan_desc = clean($conn,$_POST['loan_desc']);
  $period_time = strtotime(str_replace('/', '-', $loan_date));
  $period_day = date('d',$period_time);
  $period_month = date('m',$period_time);
  $period_year = date('Y',$period_time);
  $time = time();

  if($duration_type == 1){
    $duration = $duration;
  }elseif($duration_type == 2){
    $duration = ($duration * 12);
  }

  //Set default description
  ($loan_desc == "") ? $loan_desc = 'Loan' : $loan_desc = $loan_desc;

  //add rate and duration to principal
  $pay_amount = ((($loan_amount + $duration) * $per_rate) + $loan_amount) + LOAN_TOPUP; 

  if (!isset($_GET['loanid'])) {

    if ($member_id == "" || $loan_amount == "" || $duration == "" || $loan_date == "") {
      
      setNotification(2,$lang['blank_input_error']);
          
    }elseif(!is_numeric($pay_amount)){
      
      setNotification(2,$lang['is_numeric_error']);
      
    }else{
        
      $sql = "INSERT INTO loan_account ".
      "(member_id, staff_id, payment_name, duration, duration_left, credit, debit, description, period_day, period_month, period_year, date, status, time) ".
      "VALUES ".
      "('$member_id','$staff_id','$paymentname','$duration','$duration','0','$pay_amount','$loan_desc','$period_day','$period_month','$period_year','$loan_date','0','$time')";
      $retval = mysqli_query($conn,$sql);
      mysqliDie($retval);  
      
      //insert history
      $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      addLog($conn,$logger,'An amount of GHC "'.$loan_amount.'" was paid to "'.$staff_id.'" as loan for '.$duration.' months');
          
      setNotification(1,$lang['save_success']);        

    }

  }elseif(isset($_GET['loanid'])){

    //

  }
}
?>