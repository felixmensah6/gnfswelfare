<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['expensetypeid'])){
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM expense_type WHERE id="'.$_GET['expensetypeid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'The expense type "'.itemInfo($conn,'expense_type','id',$_GET['expensetypeid'],'expense').'" was deleted from the system.');
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);
}
?>