<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when button is clicked
if (isset($_POST['save'])) {
    
	//form variables
	$amount_charged = clean($conn,$_POST['amount_charged']);
	$expenses_made = clean($conn,$_POST['expenses_made']);
	$eng_date = clean($conn,$_POST['eng_date']);
	$eng_desc = clean($conn,$_POST['eng_desc']);
	$eng_day = date('j',time());
	$eng_month = date('n',time());
	$eng_year = date('Y',time());
	$eng_time = time();

	($eng_desc == "") ? $eng_desc = 'Investment' : $eng_desc = $eng_desc;

	if (!isset($_GET['engagementid'])) {

		if ($amount_charged == "" || $eng_date == "") {
			
			setNotification(2,'',$lang['blank_input_error']);
					
		}elseif(!is_numeric($amount_charged) || !is_numeric($expenses_made)){
			
			setNotification(2,'',$amount_charged);
			
		}elseif($amount_charged <= 0){
			
			setNotification(2,'',$lang['no_amount_error'],' for Amount');
			
		}else{
				
			$sql = "INSERT INTO band_account ".
			"(credit, debit, date, description, eng_day, eng_month, eng_year, time) ".
			"VALUES ".
			"('$amount_charged','$expenses_made','$eng_date','$eng_desc','$eng_day','$eng_month','$eng_year','$eng_time')";
			$retval = mysqli_query($conn,$sql);
			mysqliDie($retval);	
			
			//insert history
			$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
			addLog($conn,$logger,'Investment: Amount GHC "'.$amount_charged.'" added.');
			
			setNotification(1,'',$lang['save_success']);

		}

	}elseif(isset($_GET['engagementid'])){

		if ($amount_charged == "" || $eng_date == "") {
			
			setNotification(2,'',$lang['blank_input_error']);
					
		}elseif(!is_numeric($amount_charged) || !is_numeric($expenses_made)){
			
			setNotification(2,'',$lang['is_numeric_error']);
			
		}elseif($amount_charged <= 0){
			
			setNotification(2,'',$lang['no_amount_error'],' for Amount');
			
		}else{
				
			$sql = "UPDATE band_account SET credit='".$amount_charged."',debit='".$expenses_made."',description='".$eng_desc."',eng_day='".$eng_day."',eng_month='".$eng_month."',eng_year='".$eng_year."',date='".$eng_date."' WHERE id='".$_GET['engagementid']."'";
			$retval = mysqli_query($conn,$sql);
			mysqliDie($retval);	
			
			//insert history
			$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
			addLog($conn,$logger,'Investment updated: Amount GHC "'.$amount_charged.'" updated.');
			
			setNotification(1,'',$lang['update_success']);

		}

	}
}
?>