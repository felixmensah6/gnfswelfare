<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $filename = $_FILES['userfile']['name'];
  $filesize = $_FILES['userfile']['size'];
  $allowed_filetypes = array('.zip','.rar'); 
  $max_filesize = 10485760;  //10mb
  $upload_path = '../../';
  $ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); 
  $ext = strtolower($ext); 

  if($filename == ''){
    
    setNotification(2,$lang['update_file_error']);
    
  }elseif($filename != '' && $filesize > $max_filesize){
    
    setNotification(2,$lang['filesize_error'],($max_filesize / 1048576).'mb.');
    
  }elseif($filename != '' && !in_array($ext,$allowed_filetypes)){
    
    setNotification(2,$lang['update_filetype_error']);
    
  }else{
       
    // Upload the file to your specified path.
    $new_file_name = md5(time()).$ext;
    move_uploaded_file($_FILES['userfile']['tmp_name'],$upload_path . $new_file_name);

    //Extract zipped content
    $zip = new ZipArchive;
    $res = $zip->open('../../'.$new_file_name);
    if ($res === TRUE) {
       $zip->extractTo('../../');
       $zip->close();

       //Delete update file
       unlink($upload_path . $new_file_name);

      //insert history
      //$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
      //addLog($conn,$logger,'A new member "'.$firstname.' '.$lastname.'" was added to the Database.');
          
      setNotification(1,$lang['install_update_success']);

    } else {

      setNotification(2,$lang['install_update_error']);

    }
    
  }
}
?>