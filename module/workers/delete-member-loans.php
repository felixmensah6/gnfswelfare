<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['loanid'])){

	$loan_name = getPeriod(itemInfo($conn,'loan_account','loan_id',$_GET['loanid'],'payment_name'));
	$staffid = itemInfo($conn,'loan_account','loan_id',$_GET['loanid'],'staff_id');
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM loan_account WHERE loan_id="'.$_GET['loanid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'The user deleted '.$loan_name.' for member with Staff ID '.$staffid);
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);

}
?>