<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM social_account";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['staff_id'].'",
		      "'.userTitle(itemInfo($conn,'members','member_id',$row['member_id'],'gender')).' '.ucwords(strtolower($row['account_holder'])).'",
		      "'.number_format($row['debit'],2).'",
		      "'.number_format($row['credit'],2).'",
		      "'.number_format(($row['credit'] - $row['debit']),2).'",
		      "'.paymentDuration($row['months']).'",
		      "<a href=\"?page=members&subpage=manage+members&useraction=details&section=social&memberid='.$row['member_id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-search\"></i> Details</a>"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>