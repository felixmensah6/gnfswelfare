<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM logs ORDER BY log_id DESC";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['username'].'",
		      "'.clean($conn,$row['activity']).'",
		      "'.date('j-m-Y g:i:s A',$row['log_time']).'",
		      "'.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['log_id'].'\" onclick=\"confirmDelete('. "'del" . $row['log_id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-logs.php?logid=" . $row['log_id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>