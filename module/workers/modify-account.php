<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when the button is clicked
if (isset($_POST['save'])) {
		
	//form variables
	$firstname = clean($conn,$_POST['firstname']);
	$lastname = clean($conn,$_POST['lastname']);
	$email = "";
	
	if(!empty($_FILES['userfile']['name'])){
		$filename = $_FILES['userfile']['name'];
		//calculate dimension
		list($width, $height) = getimagesize($_FILES['userfile']['tmp_name']); 
	}else{
		$filename = '';
	}
	
	$filesize = $_FILES['userfile']['size'];
	$allowed_filetypes = array('.jpg','.gif','.png'); 
	$max_filesize = 5242880;  //5mb
	$upload_path = '../content/images/tmp/';
	$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); 
	$ext = strtolower($ext); 

	if ($firstname == "" || $lastname == "") {
		
		setNotification(2,'',$lang['blank_input_error']);
				
	}elseif($email != "" && !preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)){
		
		setNotification(2,'',$lang['email_error']);
		
	}elseif($filename != '' && $filesize > $max_filesize){
		
		setNotification(2,'',$lang['filesize_error'],($max_filesize / 1048576).'mb.');
		
	}elseif($filename != '' && ($width < 500 || $height < 500)){
		
		setNotification(2,'',$lang['image_dimension_error']);
		
	}elseif($filename != '' && !is_writable($upload_path)){
		
		setNotification(2,'',$lang['folder_write_error']);
		
	}elseif($filename != '' && !in_array($ext,$allowed_filetypes)){
		
		setNotification(2,'',$lang['image_ext_error']);
		
	}else{
		
		if($filename != ''){
		
			  // Upload the file to your specified path.
			  $new_file_name = md5(itemInfo($conn,'users','user_id',UserID(),'username')).$ext;
			  move_uploaded_file($_FILES['userfile']['tmp_name'],$upload_path . $new_file_name);
			  
			  // *** Include the class
			  include("../class/image-resize.class.php");
			  
			  // *** 1) Initialise / load image
			  $resizeObj = new resize($upload_path . $new_file_name);
		  
			  // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
			  $resizeObj -> resizeImage(500,500,'crop');
		  
			  // *** 3) Save image
			  $resizeObj -> saveImage('../content/images/users/'.$new_file_name, 100);
			  
			  // *** 4) Delete original image & existing image
			  unlink($upload_path . $new_file_name);

		}

		//update database with new data if everything is OK!
		($filename == "") ?  $new_file_name = "" : $new_file_name = ",avatar='".$new_file_name."'";
		
		$sql = "UPDATE users SET firstname='".$firstname."',lastname='".$lastname."',email='".$email."' ".$new_file_name." WHERE user_id='".UserID()."'";
		$updateuser = mysqli_query($conn,$sql);
		mysqliDie($updateuser);
				
		setNotification(1,'',$lang['update_success']); 

	}
}


////////////CHANGE PASSWORD///////////////////////////
if (isset($_POST['savepass'])) {
	
	//form variables
	$password = clean($conn,$_POST['password']);
	$password1 = clean($conn,$_POST['password1']);
	$password2 = clean($conn,$_POST['password2']);
	
	if ($password == "" || $password1 == "" || $password2 == "") {
			
		setNotification(2,'',$lang['blank_input_error']);
					
	}elseif(sha1($password) != itemInfo($conn,'users','user_id',UserID(),'password')){
		
		setNotification(2,'',$lang['old_password_error']);
		
	}elseif(strlen($password1) < 8 || strlen($password1) > 30){
				
		setNotification(2,'',$lang['password_length_error'],'8 or more characters long.');
			
	}elseif($password1 != $password2){
			
		setNotification(2,'',$lang['password_match_error']);
			
	}else{
		
		$sql = "UPDATE users SET password='".sha1($password1)."' WHERE user_id='".UserID()."'";
		$updateuser = mysqli_query($conn,$sql);
		mysqliDie($updateuser);
				
		setNotification(1,'',$lang['update_success']); 
		
	}
}
?>