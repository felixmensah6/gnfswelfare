<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM members WHERE status=1 ORDER BY staff_id";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['staff_id'].'",
		      "'.userTitle($row['gender']).' '.ucwords(strtolower($row['firstname'].' '.$row['lastname'])).'",
		      "'.ucwords(strtolower(itemInfo($conn,'stations','station_id',$row['station'],'station_name'))).'",
		      "'.userGender($row['gender']).'",
		      "<a href=\"?page=members&subpage=manage+members&useraction=details&section=overview&memberid='.$row['member_id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-search\"></i> View</a> '.((checkAccess('2',$_SESSION['user_perms'])) ? '<a href=\"?page=members&subpage=manage+members&useraction=edit&memberid='.$row['member_id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-pencil\"></i> Edit</a>' : "" ).' '.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" id=\"del'.$row['member_id'].'\" onclick=\"confirmDelete('. "'del" . $row['member_id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-members.php?memberid=" . $row['member_id'] . "'" . ');return false;\" class=\"btn btn-default btn-xs\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>