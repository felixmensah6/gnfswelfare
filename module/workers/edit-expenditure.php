<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

// when the button is clicked
if (isset($_POST['save'])) {
    
  //form variables
  $exp_name = clean($conn,$_POST['exp_name']);
  $exp_amount = clean($conn,$_POST['exp_amount']);
  $exp_date = clean($conn,$_POST['exp_date']);
  $exp_desc = clean($conn,$_POST['exp_desc']);
  $exp_sys_date = date('m/d/Y',time());
  $expense_name = ucwords(strtolower(itemInfo($conn,'expense_type','id',$exp_name,'expense')));
  
  ($exp_date == "") ? $exp_date = $exp_sys_date : $exp_date = $exp_date; //use default date or user date
  ($exp_desc == "") ? $exp_desc = "Expenditure" : $exp_desc = $exp_desc; //use default description

  if ($exp_name == "" || $exp_amount == "") {
    
    setNotification(2,$lang['blank_input_error']);
        
  }elseif($exp_amount <= 0){
    
    setNotification(2,$lang['no_amount_error']);
    
  }elseif(!is_numeric($exp_amount)){
    
    setNotification(2,$lang['is_numeric_error']);
    
  }else{
  

    $sql = "UPDATE expenditure SET exp_name='".$exp_name."',exp_amount='".$exp_amount."',exp_desc='".$exp_desc."',exp_date='".$exp_date."' WHERE exp_id='".$_GET['expid']."'";
    $retval = mysqli_query($conn,$sql);
    mysqliDie($retval);  
    
    //insert history
    $logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
    addLog($conn,$logger,'"'.$expense_name.'" expense of "GHC '.$exp_amount.'" was updated.');
    
    setNotification(1,$lang['update_success']);  

  }
}

?>