<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM event_type ORDER BY id";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.ucwords(strtolower($row['event'])).'",
		      "'.((checkAccess('2',$_SESSION['user_perms'])) ? '<a href=\"?page=management&subpage=setup+social+events&useraction=edit&eventid='.$row['id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-pencil\"></i> Edit</a>' : '').' '.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['id'].'\" onclick=\"confirmDelete('. "'del" . $row['id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-event-type.php?eventid=" . $row['id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}

echo implode(',',$raw);

echo ']}';

?>

