<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//delete from database
if(isset($_GET['userid'])){
	
	$del_user = itemInfo($conn,'users','user_id',$_GET['userid'],'firstname').' '.itemInfo($conn,'users','user_id',$_GET['userid'],'lastname');
	
	//delete profile photo
	/*if(itemInfo($conn,'users','user_id',$_GET['userid'],'avatar') != NULL){
	  unlink('img/users/large/'.itemInfo($conn,'users','user_id',$_GET['userid'],'avatar'));
	  unlink('img/users/small/'.itemInfo($conn,'users','user_id',$_GET['userid'],'avatar'));
	}*/
	
	//delete from table name and row identifier
	$sql = 'DELETE FROM users WHERE user_id="'.$_GET['userid'].'"';
	$retval = mysqli_query($conn,$sql);
	mysqliDie($retval);
	
	//insert history
	$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
	addLog($conn,$logger,'The user "'.$del_user.'" was deleted from the system.');
	
	//close connection
	mysqli_close($conn);
	
	setNotification(1,$lang['delete_success']);
}
?>