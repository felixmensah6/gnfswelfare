<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when button is clicked
if (isset($_POST['save'])) {
    
	//form variables
	$event_name = clean($conn,$_POST['event_name']);
	$amount_given = clean($conn,$_POST['amount_given']);
	$event_date = clean($conn,$_POST['event_date']);
	$event_desc = clean($conn,$_POST['event_desc']);
	$member_id = $_GET['memberid'];
	$fullname = ucwords(strtolower(itemInfo($conn,'members','member_id',$member_id,'firstname').' '.itemInfo($conn,'members','member_id',$member_id,'lastname')));
	$time = time();

	($event_desc == "") ? $event_desc = 'Event' : $event_desc = $event_desc;

	if (!isset($_GET['eventid'])) {

		if ($amount_given == "" || $event_name == "" || $event_date == "") {
			
			setNotification(2,$lang['blank_input_error']);
					
		}elseif(!is_numeric($amount_given)){
			
			setNotification(2,$lang['is_numeric_error']);
			
		}elseif($amount_given <= 0){
			
			setNotification(2,$lang['no_amount_error']);
			
		}else{
				
			$sql = "INSERT INTO social_event ".
			"(member_id, event_name, amount_given, event_date, event_desc, event_time) ".
			"VALUES ".
			"('$member_id','$event_name','$amount_given','$event_date','$event_desc','$time')";
			$retval = mysqli_query($conn,$sql);
			mysqliDie($retval);	
			
			//insert history
			$logger = ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname')));
			addLog($conn,$logger,'An amount of GHC "'.$amount_given.'" was paid to "'.$fullname.'". for "'.$event_name.'"');
			
			setNotification(1,$lang['save_success']);

		}

	}elseif(isset($_GET['eventid'])){

		//

	}
}
?>