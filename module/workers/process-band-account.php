<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM band_account ORDER BY id DESC";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.$row['description'].'",
		      "'.number_format($row['credit'],2).'",
		      "'.number_format($row['debit'],2).'",
		      "'.number_format(($row['credit'] - $row['debit']),2).'",
		      "'.dateFormat($row['date'],'DD-MM-YYYY',1).'",
		      "<a href=\"?page=accounts&subpage=band&useraction=edit&engagementid='.$row['id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-pencil\"></i> Edit</a> <a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['id'].'\" onclick=\"confirmDelete('. "'del" . $row['id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-band-account.php?engagementid=" . $row['id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>