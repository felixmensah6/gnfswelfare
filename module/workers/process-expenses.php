<?php

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');

echo '{"data": [';

	//show records
	$sql = "SELECT * FROM expenditure ORDER BY exp_id DESC";
	$query = mysqli_query($conn,$sql);
	mysqliDie($conn,$query);
	$raw = array();
	$count = 1;
	while ($row = mysqli_fetch_assoc($query)) {

		$raw[] = '
		    [
		      "'.$count++.'",
		      "'.ucwords(strtolower(itemInfo($conn,'expense_type','id',$row['exp_name'],'expense'))).'",
		      "'.number_format($row['exp_amount'],2).'",
		      "'.dateFormat($row['exp_date'],'DD-MM-YYYY').'",
		      "'.ucfirst(strtolower($row['exp_desc'])).'",
		      "'.date('j-m-Y',$row['exp_time']).'",
		      "'.((checkAccess('2',$_SESSION['user_perms'])) ? '<a href=\"?page=expenses&subpage=manage+expenses&useraction=edit&expid='.$row['exp_id'].'\" class=\"btn btn-default btn-xs\"><i class=\"icon-pencil\"></i> Edit</a>' : '').' '.((checkAccess('3',$_SESSION['user_perms'])) ? '<a href=\"#\" class=\"btn btn-default btn-xs\" id=\"del'.$row['exp_id'].'\" onclick=\"confirmDelete('. "'del" . $row['exp_id'] . "'" . ','. "'".APP_URL.WORK_PATH."delete-expenses.php?expid=" . $row['exp_id'] . "'" . ');return false;\"><i class=\"icon-trash-o\"></i> Delete</a>' : '').'"
		    ]
		';

	}


echo implode(',',$raw);

echo ']}';

?>