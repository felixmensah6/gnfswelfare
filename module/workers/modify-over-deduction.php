<?php
//include the configuration and functions
require_once('../include/session.php');
require_once('../include/dbconnect.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

//when button is clicked
if (isset($_POST['save'])) {

  //form variables
  $member_id = clean($conn,$_POST['account_holder']);
  $staff_id = itemInfo($conn,'members','member_id',$member_id,'staff_id');
  $fullname = ucwords(strtolower(itemInfo($conn,'members','member_id',$member_id,'firstname').' '.itemInfo($conn,'members','member_id',$member_id,'lastname')));
  $period = clean($conn,$_POST['period']);
  $payment_type = clean($conn,$_POST['payment_type']);
  $amount = clean($conn,$_POST['amount']);
  $description = clean($conn,$_POST['payment_desc']);
  $period_time = strtotime(str_replace('/', '-', $period));
  $period_day = date('d',$period_time);
  $period_month = date('m',$period_time);
  $period_year = date('Y',$period_time);
  $date = date('d/m/Y',time());
  $time = time();

  if($payment_type == 1){
    $credit = $amount;
    $debit = 0;
  }elseif($payment_type == 2){
    $credit = 0;
    $debit = $amount;
  }

  ($description == "") ? $description = 'Payment' : $description = $description;

  if ($member_id == "" || $amount == "" || $period== "") {

    setNotification(2,$lang['blank_input_error']);

  }elseif(!is_numeric($amount)){

  setNotification(2,$lang['is_numeric_error']);

  }elseif($amount <= 0){

    setNotification(2,$lang['no_amount_error']);

  }else{

    //CREDIT
    if($payment_type == 1) {

      //if results exist
      $qry = "SELECT * FROM over_deduction_account WHERE member_id='".$member_id."'";
      $result = mysqli_query($conn,$qry);

      if(mysqli_num_rows($result) == 1) {

        $sql = "UPDATE over_deduction_account SET months=months + 1,credit=credit + ".$amount.",debit=debit + 0,date='$date',time='$time' WHERE member_id=".$member_id."";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);

        $sql = "INSERT INTO over_deduction_details ".
        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
        "VALUES ".
        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);

        setNotification(1,$lang['save_success']);

      }else{

        $sql = "INSERT INTO over_deduction_account ".
        "(member_id, staff_id, account_holder, months, credit, debit, date, time) ".
        "VALUES ".
        "('$member_id','$staff_id','$fullname','1','$credit','$debit','$date','$time')";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);

        $sql = "INSERT INTO over_deduction_details ".
        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
        "VALUES ".
        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);

        setNotification(1,$lang['save_success']);

      }

    //DEBIT
    }elseif($payment_type == 2) {

      //if results exist
      $qry = "SELECT * FROM over_deduction_account WHERE member_id='".$member_id."'";
      $result = mysqli_query($conn,$qry);

      if(mysqli_num_rows($result) == 1) {

        $sql = "UPDATE over_deduction_account SET months=months,credit=credit + 0,debit=debit + ".$amount.",date='$date',time='$time' WHERE member_id=".$member_id."";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);

        $sql = "INSERT INTO over_deduction_details ".
        "(member_id, staff_id, period, credit, debit, description, period_day, period_month, period_year, date, time) ".
        "VALUES ".
        "('$member_id','$staff_id','$period','$credit','$debit','$description','$period_day','$period_month','$period_year','$date','$time')";
        $retval = mysqli_query($conn,$sql);
        mysqliDie($retval);

        setNotification(1,$lang['save_success']);

      }else{

        setNotification(2,$lang['no_credit_error']);

      }

    }

  }
}
?>
