    <!--Profile Box Start-->
    <div class="profile-header">
      <div class="row m-0">
        <div class="col-sm-3 p-0">
          <div class="profile-photo">
            <img src="<?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'photo') != '') ? CONT_PATH.'images/members/'.itemInfo($conn,'members','member_id',$_GET['memberid'],'photo') : IMG_PATH.'assets/images/avatar.png'; ?>" alt="profile photo">
          </div>
        </div>
        <div class="col-sm-9 p-0">
          <div class="profile-actions">

            <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
            <a href="?page=payment&subpage=make+payment&memberid=<?php echo $_GET['memberid']; ?>" class="btn btn-sm btn-success"><i class="icon-dollar"></i> Make Payment</a>
            <a href="?page=accounts&subpage=loans+and+recovery&useraction=add&memberid=<?php echo $_GET['memberid']; ?>" class="btn btn-sm btn-success"><i class="icon-money"></i> Grant Loan</a>
            <a href="?page=members&subpage=manage+members&useraction=details&section=events&sectionaction=add&memberid=<?php echo $_GET['memberid']; ?>" class="btn btn-sm btn-primary"><i class="icon-calendar"></i> Add Event</a>
            <?php } ?>
            <div class="btn-group">
              <button type="button" class="btn btn-sm btn-system dropdown-toggle" data-toggle="dropdown">
                More
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu dropdown-menu-right" role="menu">
                <li><a href="?page=members&subpage=manage+members&useraction=print&memberid=<?php echo $_GET['memberid']; ?>&userid=<?php echo UserID(); ?>"><i class="icon-print mr-5"></i> Print Statement</a></li>
                <li><a href="#"><i class="icon-camera mr-5"></i> Snap Photo</a></li>
                <li class="divider"></li>
                <?php if(itemInfo($conn,'members','member_id',$_GET['memberid'],'status') == 1){ ?>
                <li><a href="?page=members&subpage=archive&memberid=<?php echo $_GET['memberid']; ?>"><i class="icon-archive mr-5"></i> Send to Archive</a></li>
                <?php }else{ ?>
                <li><a href="?page=members&subpage=archive&memberid=<?php echo $_GET['memberid']; ?>"><i class="icon-archive mr-5"></i> Restore Member</a></li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <div class="profile-details">
            <h2><?php echo userTitle(itemInfo($conn,'members','member_id',$_GET['memberid'],'gender')) .' '. ucwords(strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'firstname').' '.itemInfo($conn,'members','member_id',$_GET['memberid'],'lastname'))); ?></h2>
            <p><strong>Staff ID:</strong> <?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'staff_id'); ?> <?php echo memberStatus(itemInfo($conn,'members','member_id',$_GET['memberid'],'status')); ?>
            <p><strong>Station:</strong> <?php echo ucwords(strtolower(itemInfo($conn,'stations','station_id',itemInfo($conn,'members','member_id',$_GET['memberid'],'station'),'station_name'))); ?></p>
            <p><strong>Age:</strong> <?php echo getAge(itemInfo($conn,'members','member_id',$_GET['memberid'],'birthday')); ?></p>
          </div>
        </div>
      </div>
    </div>
    <!--Profile Box End-->

    <!--Page Nav Tab Start-->
    <ul class="nav nav-tabs page-nav-tab nav-right" role="tablist">
      <li <?php echo activeChildNav('events',$_GET['section']); ?>><a href="?page=members&subpage=manage+members&useraction=details&section=events&memberid=<?php echo $_GET['memberid']; ?>">Events</a></li>
      <li <?php echo activeChildNav('loans',$_GET['section']); ?>><a href="?page=members&subpage=manage+members&useraction=details&section=loans&memberid=<?php echo $_GET['memberid']; ?>">Loans</a></li>
      <li <?php echo activeChildNav('social',$_GET['section']); ?>><a href="?page=members&subpage=manage+members&useraction=details&section=social&memberid=<?php echo $_GET['memberid']; ?>">Funeral & Social</a></li>
      <li <?php echo activeChildNav('welfare',$_GET['section']); ?>><a href="?page=members&subpage=manage+members&useraction=details&section=welfare&memberid=<?php echo $_GET['memberid']; ?>">Welfare</a></li>
      <li <?php echo activeChildNav('overview',$_GET['section']); ?>><a href="?page=members&subpage=manage+members&useraction=details&section=overview&memberid=<?php echo $_GET['memberid']; ?>">Overview</a></li>
    </ul>
    <!--Page Nav Tab End-->

<?php if($_GET['section'] == 'overview'){ ?>

    <!--Row Start-->
    <div class="row mb-10">
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format(itemInfo($conn,'welfare_account','member_id',$_GET['memberid'],'credit') - itemInfo($conn,'welfare_account','member_id',$_GET['memberid'],'debit'),2); ?></h3>
          <p>Welfare Dues</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format(itemInfo($conn,'social_account','member_id',$_GET['memberid'],'credit') - itemInfo($conn,'social_account','member_id',$_GET['memberid'],'debit'),2); ?></h3>
          <p>Funeral & Social Dues</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format(sumSpecificColumn($conn,'amount_given','social_event','member_id',$_GET['memberid']),2) ; ?></h3>
          <p>Funded Events</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format(itemInfo($conn,'over_deduction_account','member_id',$_GET['memberid'],'credit') - itemInfo($conn,'over_deduction_account','member_id',$_GET['memberid'],'debit'),2); ?></h3>
          <p>Over Deductions</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format(customQuery($conn,"SELECT SUM(debit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(debit)"),2) ; ?></h3>
          <p>Loans</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format(customQuery($conn,"SELECT SUM(credit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(credit)"),2) ; ?></h3>
          <p>Recovered (<?php echo @number_format((customQuery($conn,"SELECT debit FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit LIMIT 1","debit") / customQuery($conn,"SELECT duration FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit LIMIT 1","duration")),2) ; ?> / Month)</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <h3><?php echo APP_CUR . number_format((customQuery($conn,"SELECT SUM(debit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(debit)") - customQuery($conn,"SELECT SUM(credit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(credit)")),2) ; ?></h3>
          <p>Loan Arrears</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-default bg-default profile-stats">
          <?php
            $percentage = @(customQuery($conn,"SELECT SUM(credit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(credit)") / customQuery($conn,"SELECT SUM(debit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(debit)"));
            $percentage = ($percentage * 100);
          ?>
          <div class="progress" style="margin: 5px 0 11px">
            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo floor($percentage); ?>%;">
              <?php echo floor($percentage); ?>%
            </div>
          </div>
          <p>Recovery Progress</p>
        </div>
      </div>
    </div>
    <!--Row End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Personal Details</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('2',$_SESSION['user_perms'])){ ?>
                <a href="?page=members&subpage=manage+members&useraction=edit&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt"><i class="icon-pencil"></i> Edit Details</a>
                <?php } ?>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Date of Birth:</label>
                  <div class="col-sm-3">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'birthday') == "") ? "<em>None</em>" : itemInfo($conn,'members','member_id',$_GET['memberid'],'birthday'); ?></p>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Gender:</label>
                  <div class="col-sm-4">
                    <p class="form-control-static"><?php echo userGender(itemInfo($conn,'members','member_id',$_GET['memberid'],'gender')); ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Marital Status:</label>
                  <div class="col-sm-3">
                    <p class="form-control-static"><?php echo maritalStatus(itemInfo($conn,'members','member_id',$_GET['memberid'],'marital_status')); ?></p>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Name of Spouse:</label>
                  <div class="col-sm-5">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'spouse_name') == "") ? "<em>None</em>" : ucwords(strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'spouse_name'))); ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">No. of Children:</label>
                  <div class="col-sm-3">
                    <p class="form-control-static"><?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'children'); ?></p>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Contact Number:</label>
                  <div class="col-sm-5">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'contact') == "") ? "<em>None</em>" : itemInfo($conn,'members','member_id',$_GET['memberid'],'contact'); ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Father's Name:</label>
                  <div class="col-sm-3">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'father_name') == "") ? "<em>None</em>" : ucwords(strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'father_name'))); ?></p>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Mother's Name:</label>
                  <div class="col-sm-5">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'mother_name') == "") ? "<em>None</em>" : ucwords(strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'mother_name'))); ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Postal Address:</label>
                  <div class="col-sm-3">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'address') == "") ? "<em>None</em>" : ucwords(strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'address'))); ?></p>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Email:</label>
                  <div class="col-sm-5">
                    <p class="form-control-static"><?php echo (itemInfo($conn,'members','member_id',$_GET['memberid'],'email') == "") ? "<em>None</em>" : strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'email')); ?></p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Over Deductions</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=payment&subpage=make+payment&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Make Payment</a>
                <?php } ?>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="stripe"  id="over-deduction-details" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Period</th>
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Date</th> 

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Period</th>
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Date</th> 

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

    <script type="text/javascript">
    $(document).ready(function(){

      // Basic Datatable
      $('#over-deduction-details').dataTable( {
          "ajax": "<?php echo WORK_PATH . 'process-over-deduction-details.php?memberid='.$_GET['memberid']; ?>",
          stateSave: true
      });

    });
    </script>

<?php }elseif($_GET['section'] == 'welfare'){ ?>

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Welfare Contributions</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=payment&subpage=make+payment&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Make Payment</a>
                <?php } ?>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="stripe" id="welfare-details" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Period</th>
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Period</th>
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

    <script type="text/javascript">
    $(document).ready(function(){

      // Basic Datatable
      $('#welfare-details').dataTable( {
          "ajax": "<?php echo WORK_PATH . 'process-welfare-details.php?memberid='.$_GET['memberid']; ?>",
          stateSave: true
      });

    });
    </script>

<?php }elseif($_GET['section'] == 'social'){ ?>

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Funeral & Social Event Contributions</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=payment&subpage=make+payment&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Make Payment</a>
                <?php } ?>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body"> 
              <table class="stripe" id="social-details" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Period</th>                         
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Period</th>                         
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

    <script type="text/javascript">
    $(document).ready(function(){

      // Basic Datatable
      $('#social-details').dataTable( {
          "ajax": "<?php echo WORK_PATH . 'process-social-details.php?memberid='.$_GET['memberid']; ?>",
          stateSave: true
      });

    });
    </script>

<?php }elseif($_GET['section'] == 'loans'){ ?>

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Loans Acquired</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=accounts&subpage=loans+and+recovery&useraction=add&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Grant Loan</a>
                <?php } ?>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body"> 
              <table class="stripe" id="member-loans" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Loan Name</th>
                          <th>Amount</th>
                          <th>Recovered</th>
                          <th>Duration</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Loan Name</th>
                          <th>Amount</th>
                          <th>Recovered</th>
                          <th>Duration</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Recovery Payments</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=payment&subpage=make+payment&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Make Payment</a>
                <?php } ?>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="stripe" id="recovery-details" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Payment Name</th>
                          <th>Amount</th>
                          <th>Period</th>
                          <th>Remaining</th>
                          <th>Description</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Payment Name</th>
                          <th>Amount</th>
                          <th>Period</th>
                          <th>Remaining</th>
                          <th>Description</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

    <script type="text/javascript">
    $(document).ready(function(){

      // Basic Datatable
      $('#member-loans').dataTable( {
          "ajax": "<?php echo WORK_PATH . 'process-member-loans.php?memberid='.$_GET['memberid']; ?>",
          stateSave: true 
      });

      $('#recovery-details').dataTable( {
          "ajax": "<?php echo WORK_PATH . 'process-recovery-details.php?memberid='.$_GET['memberid']; ?>",
          stateSave: true
      });

    });
    </script>

<?php }elseif($_GET['section'] == 'events'){ ?>

    
    <?php if(isset($_GET['sectionaction']) && $_GET['sectionaction'] == 'add'){ ?>

        <!--Row Start-->
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <div class="panel panel-light">
              <div class="panel-heading">
                <span class="panel-icon"><i class="icon-plus"></i></span>
                <h3 class="panel-title">Add Event</h3>
                <span class="panel-controls">
                  <div class="btn-group">
                    <a href="?page=members&subpage=manage+members&useraction=details&section=events&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Manage Events</a>
                  </div>
                </span>
              </div>
              <div class="panel-collapse">
                <div class="panel-body panel-form">
                  <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-event.php?memberid='.$_GET['memberid']; ?>" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="input" class="col-sm-2 control-label">Event Name *</label>
                      <div class="col-sm-5">
                        <select class="form-control selectize" name="event_name" required>
                          <?php eventNameSelect($conn); ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="input" class="col-sm-2 control-label">Amount *</label>
                      <div class="col-sm-3">
                        <div class="input-group" data-trigger="spinner">
                          <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                          <input type="text" class="form-control" name="amount_given" required placeholder="0.00" data-step="1" data-rule="currency">
                          <span class="input-group-addon">
                            <i class="icon-sort-asc spin-up" data-spin="up"></i>
                            <i class="icon-sort-desc spin-down" data-spin="down"></i>
                          </span>
                        </div>
                      </div>
                      <label for="input" class="col-sm-2 control-label">Date</label>
                      <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="event_date" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="input" class="col-sm-2 control-label">Description</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" rows="2" name="event_desc" placeholder="Description (optional)"></textarea>
                        <div class="help-block sticky-help">
                          <strong>Hint:</strong> The system will generate a description if left blank.
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 pt-20">
                        <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                      </div>
                    </div> 
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Row End-->

    <?php }else{ ?>

        <!--Row Start-->
        <div class="row">
          <div class="col-sm-12">
            <div class="panel panel-light">
              <div class="panel-heading">
                <h3 class="panel-title">Funeral & Social Activities</h3>
                <span class="panel-controls">
                  <div class="btn-group">

                    <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                    <a href="?page=members&subpage=manage+members&useraction=details&section=events&sectionaction=add&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt">Add Event</a>
                    <?php } ?>
                  </div>
                </span>
              </div>
              <div class="panel-collapse">
                <div class="panel-body">
                  <table class="stripe" id="member-events" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Name of Event</th>
                              <th>Amount Paid</th>
                              <th>Description</th>
                              <th>Date</th>

                              <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                              <th>Actions</th>
                              <?php } ?>
                              </tr>
                      </thead>
                   
                      <tfoot>
                          <tr>
                              <th>#</th>
                              <th>Name of Event</th>
                              <th>Amount Paid</th>
                              <th>Description</th>
                              <th>Date</th>

                              <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                              <th>Actions</th>
                              <?php } ?>
                          </tr>
                      </tfoot>
                   
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Row End-->

        <script type="text/javascript">
        $(document).ready(function(){

          // Basic Datatable
          $('#member-events').dataTable( {
              "ajax": "<?php echo WORK_PATH . 'process-member-events.php?memberid='.$_GET['memberid']; ?>",
              stateSave: true 
          });

        });
        </script>

    <?php } ?>



<?php } ?>

