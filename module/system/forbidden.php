      <!-- Modal -->
      <div class="modal fade" id="auto-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <h2 class="c-danger"><i class="icon-lock"></i> Access Denied!</h2>
              <p>You do not have permission to access this section. Contact your Administrator for help.</p>
            </div>
            <div class="modal-footer">
              <button type="button" onclick="history.go(-1)" class="btn btn-danger">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->

<!--Include JS-->
<script type="text/javascript">
$(document).ready(function(){
    
  // Auto Load Modal
  $('#auto-modal').modal('show');
  
});
</script>
<!--Include JS-->