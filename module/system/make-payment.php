<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->
   
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-plus"></i></span>
            <h3 class="panel-title">Make Payment</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <?php if(isset($_GET['memberid'])){ ?>
                <a href="?page=members&subpage=manage+members&useraction=details&section=overview&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt"><i class="icon-arrow-circle-left"></i> Go Back</a>
                <?php } ?>

                <?php if(checkAccess('8',$_SESSION['user_perms'])){ ?>
                <a href="#" class="pc-link-alt dropdown-toggle" data-toggle="dropdown">Accounts <i class="caret"></i></a>
                <ul class="dropdown-menu " role="menu">
                  <li><a href="?page=accounts&subpage=welfare">Welfare</a></li>
                  <li><a href="?page=accounts&subpage=funeral+and+social">Funeral & Social</a></li>
                  <li><a href="?page=accounts&subpage=band">Band</a></li>
                  <li><a href="?page=accounts&subpage=loans+and+recovery">Loans & Recovery</a></li>
                  <li><a href="?page=accounts&subpage=over+deduction">Over Deduction</a></li>
                </ul>
                <?php } ?>
                
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span> 
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-payment.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Member Name *</label>
                    <div class="col-sm-10">
                      <select name="member_id" class="form-control members" id="members" placeholder="Select User Role" required>
                        <?php 
                          $memid = (isset($_GET['memberid'])) ? $_GET['memberid'] : "";
                          memberSelect($conn,$memid); 
                        ?>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Account Name *</label>
                    <div class="col-sm-5">
                      <select name="account" id="account" class="form-control" placeholder="Account Name" required>
                        <option value="">Account Name</option>
                        <option value="1" class="other_ac">Welfare Dues</option>
                        <option value="2" class="other_ac">Funeral & Social Account</option>
                        <option value="3" id="loan_ac">Loan Account</option>
                        <option value="4" class="other_ac">Over Deduction Account</option>
                      </select>
                    </div>
                </div> 
                <div class="form-group" id="loanbox" style="display:none">
                  <label for="input" class="col-sm-2 control-label">Loan Name</label>
                    <div class="col-sm-6">
                      <?php
                        $loanname = "";
                        if(isset($_GET['memberid'])){
                          //form variables
                          $loan_memberid = (int) clean($conn,$_GET['memberid']);

                          //show records
                          $sql = "SELECT * FROM loan_account WHERE member_id=".$loan_memberid." AND credit < debit LIMIT 1";
                          $query = mysqli_query($conn,$sql);
                          mysqliDie($conn,$query);
                          $row = mysqli_fetch_assoc($query);

                          if(mysqli_num_rows($query) >= 1) {
                            $_SESSION['loan_id'] = $row['loan_id'];
                            $pay = ' - Pay: '.@number_format(($row['debit'] / $row['duration']),2);
                            $loanname = str_replace('&cent;', '¢', $row['payment_name'].$pay);
                          }else{
                            $loanname = "";
                          }
                        }
                      ?>
                      <input type="text" class="form-control" id="loans" name="loan_name" readonly value="<?php echo $loanname; ?>">
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Payment Type *</label>
                    <div class="col-sm-4">
                      <select name="payment_type" class="form-control selectize" placeholder="Payment Type" required>
                        <option value="1">Credit Account</option>
                        <option value="2">Debit Account</option>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="amount" id="amount" required placeholder="0.00" data-step="1" data-rule="currency">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Date/Period *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="date" data-date-format="dd/mm/yyyy" required placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="payment_desc" placeholder="Description (optional)"></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

  // Selectize
  $('.selectize').selectize();

  $('#members').selectize({
    onChange: function(value) {
      $.ajax({
         type: "GET",
         url: "<?php echo WORK_PATH . 'get-loans.php'; ?>",
         data: 'member_id='+ value,
         success: function(option){
          //alert(option);
          $('#loans').attr( "value", option );
      }});
    }
  });

  <?php if(isset($_GET['memberid'])){ ?>
    $('.members')[0].selectize.lock();
  <?php } ?>

  $('#account').selectize({
    onChange: function(value) {
      if(value == 3){
        $('#loanbox').slideDown();
        $( "#amount" ).attr( "value", "" );
      }else{
        $('#loanbox').slideUp();
        $( "#amount" ).attr( "value", "" );
      }
    }
  });

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true,
    startView: 1
  });

});

</script>
<!--Include JS-->