<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/css/jasny.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/blockui/css/blockui.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'edit'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('2',$_SESSION['user_perms'])){ ?>

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12"> 
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-pencil"></i></span>
            <h3 class="panel-title">Edit Member Form</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <?php if(isset($_GET['memberid'])){ ?>
                <a href="?page=members&subpage=manage+members&useraction=details&section=overview&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt"><i class="icon-arrow-circle-left"></i> Go Back</a>
                <?php } ?>

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=members&subpage=add+member" class="pc-link-alt">Add New Member</a>
                <?php } ?>

                <a href="?page=members&subpage=manage+members" class="pc-link-alt">Manage Members</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'edit-members.php?memberid='.$_GET['memberid']; ?>" enctype="multipart/form-data">
                <div class="section-divider-blank"><span>Personal Information</span></div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Full Name *</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'firstname'); ?>" name="firstname" required placeholder="First Name">
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'lastname'); ?>" name="lastname" required placeholder="Last Name">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Date of Birth</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control datepicker" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'birthday'); ?>" name="birthday" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                  </div>
                  <label for="input" class="col-sm-2 control-label">Gender *</label>
                  <div class="col-sm-4">
                    <label class="radio-inline">
                      <input type="radio" name="gender" required value="1" <?php echo  isChecked(itemInfo($conn,'members','member_id',$_GET['memberid'],'gender'),1); ?>>Male
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="gender" required value="2" <?php echo isChecked(itemInfo($conn,'members','member_id',$_GET['memberid'],'gender'),2); ?>>Female
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Marital Status</label>
                  <div class="col-sm-3">
                    <label class="radio-inline">
                      <input type="radio" name="marital_status" required value="1" <?php echo  isChecked(itemInfo($conn,'members','member_id',$_GET['memberid'],'marital_status'),1); ?>>Single
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="marital_status" required value="2" <?php echo  isChecked(itemInfo($conn,'members','member_id',$_GET['memberid'],'marital_status'),2); ?>>Married
                    </label>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Name of Spouse</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'spouse_name'); ?>" name="spouse" placeholder="Spouse Name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">No. of Children</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="children" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'children'); ?>" data-rule="quantity" data-min="0">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Contact Number</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="input-contact" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'contact'); ?>" name="contact" placeholder="Contact Number">
                    <span class="help-block">Seperate numbers with a comma sign.</span>
                  </div>
                </div>
                <div class="section-divider-blank"><span>Other Information</span></div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Staff ID *</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'staff_id'); ?>" name="staff_id" required placeholder="Staff ID">
                  </div>
                  <label for="input" class="col-sm-2 control-label">Station *</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="station" required id="select-station">
                      <?php stationSelect($conn,itemInfo($conn,'members','member_id',$_GET['memberid'],'station')); ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Father's Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'father_name'); ?>" name="father" placeholder="Father's Name">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Mother's Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'mother_name'); ?>" name="mother" placeholder="Mother's Name">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Postal Address</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'address'); ?>" name="address" placeholder="Postal Address">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'email'); ?>" name="email" placeholder="Email">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Upload Photo</label>
                  <div class="col-sm-5">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                      <div class="form-control" data-trigger="fileinput">
                        <span class="fa fa-file fileinput-exists"></span> 
                        <span class="fileinput-filename"></span>
                      </div>
                      <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select Image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="userfile">
                      </span>
                      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php 
}elseif(isset($_GET['useraction']) && $_GET['useraction'] == 'details'){

    //include page
    include(SYS_PATH.'member-details.php');

}elseif(isset($_GET['useraction']) && $_GET['useraction'] == 'print'){

?>

    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-print"></i></span>
            <h3 class="panel-title">Print Preview</h3>
            <span class="panel-controls">
              <div class="btn-group">
              <a href="javascript:;" onclick="printFrame('print-statement')" class="pc-link-alt"><i class="icon-print"></i> Print Statement</a>
                <a href="?page=members&subpage=manage+members&useraction=details&section=overview&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt"><i class="icon-arrow-circle-left"></i> Go Back</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <iframe src="<?php echo PLUGIN_PATH.'dompdf/statement/index.php?memberid='.$_GET['memberid'].'&userid='.$_GET['userid'].''; ?>" width="100%" height="768px" frameborder="0" id="print-statement"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

<?php
}else{ 
?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-users"></i></span>
            <h3 class="panel-title">All Members</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=members&subpage=add+member" class="pc-link-alt">Add New Member</a>
                <?php } ?>
                
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>
                          <th>Station</th>
                          <th>Gender</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>
                          <th>Station</th>
                          <th>Gender</th>
                          <th>Actions</th>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

<?php } ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/js/jasny.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script> 
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/blockui/js/blockui.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

  <?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'print'){ ?>
  // Block UI
  $.blockUI.defaults.css = {};
  $.blockUI();
  <?php } ?>

	// Basic Datatable
  $('.basic-datatable').dataTable( {
      "ajax": "<?php echo WORK_PATH . 'process-members.php'; ?>",
      stateSave: true
  });

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true
  });

  $('.selectize').selectize();

});

// Print iFrame Content Function
function printFrame(id){
  printPDF = document.getElementById(id);
  printPDF.contentWindow.focus();
  printPDF.contentWindow.print();
}

$(window).load(function() {
  // Hide Block UI
  $.unblockUI();
});
</script>
<!--Include JS-->