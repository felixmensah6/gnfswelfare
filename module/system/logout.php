<?php
if (isset($_SESSION['user_id'])) {
	
    // Delete the session vars by clearing the $_SESSION array
    $_SESSION = array();
	
    // Delete the session cookie by setting its expiration to an hour ago (3600)
    if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', mktime() - (60 * 60 * 24 * 30), '/');
    }

    // Destroy the session
    session_destroy();
  }

  // Redirect to the home page
  header('Location: ' . APP_URL); 

?>