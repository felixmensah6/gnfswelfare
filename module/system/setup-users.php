<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/css/jasny.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'add'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(itemInfo($conn,'users','user_id',UserID(),'userlevel') == 1 && checkAccess('1',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-plus"></i></span>
            <h3 class="panel-title">Add New User</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=management&subpage=setup+users" class="pc-link-alt">Manage Users</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-users.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Full Name *</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="text" class="form-control"  name="firstname" required placeholder="First Name">
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control"  name="lastname" required placeholder="Last Name">
                    </div>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Username *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="username" required placeholder="Username">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Password *</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="password" class="form-control"  name="password1" required placeholder="Password">
                    </div>
                    <div class="col-sm-6">
                      <input type="password" class="form-control"  name="password2" required placeholder="Confirm Password">
                    </div>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">User Role *</label>
                    <div class="col-sm-5">
                      <select name="userlevel" class="form-control selectize" placeholder="Select User Role" required>
                        <?php userRoleSelect($conn); ?>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Permissions</label>
                    <div class="col-sm-10">
                      <div class="checkbox checkall">
                        <a href="javascript:;" class="form-control-static col-sm-10 p-0" id="selectall">Select All</a>
                       <?php
                          // select values
                          $sql = 'SELECT * FROM permissions';
                          $query = mysqli_query($conn,$sql);
                          mysqliDie($query);
                          while ($row = mysqli_fetch_assoc($query)) {
                            echo '<label class="col-sm-5"><input type="checkbox" value="'.$row['perm_id'].'" name="perm[]">'.$row['perm_name'].'</label>';
                          }
                        ?>
                      </div>             
                    </div>
                </div> 
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }elseif(isset($_GET['useraction']) && $_GET['useraction'] == 'edit'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(itemInfo($conn,'users','user_id',UserID(),'userlevel') == 1 && checkAccess('2',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-pencil"></i></span>
            <h3 class="panel-title">Edit User</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=management&subpage=setup+users&useraction=add" class="pc-link-alt">Add New User</a>
                <a href="?page=management&subpage=setup+users" class="pc-link-alt">Manage Users</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-users.php?userid='.$_GET['userid']; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Full Name *</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="text" class="form-control"  name="firstname" required placeholder="First Name" value="<?php echo ($_GET['useraction'] == 'edit') ? itemInfo($conn,'users','user_id',$_GET['userid'],'firstname') : ''; ?>">
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control"  name="lastname" required placeholder="Last Name" value="<?php echo ($_GET['useraction'] == 'edit') ? itemInfo($conn,'users','user_id',$_GET['userid'],'lastname') : ''; ?>">
                    </div>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">User Role *</label>
                    <div class="col-sm-5">
                      <select name="userlevel" class="form-control selectize" placeholder="Select User Role" required>
                        <?php userRoleSelect($conn,itemInfo($conn,'users','user_id',$_GET['userid'],'userlevel')); ?>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Permissions</label>
                    <div class="col-sm-10">
                      <div class="checkbox checkall">
                        <a href="javascript:;" class="form-control-static col-sm-10 p-0" id="selectall">Select All</a>
                       <?php
                          // 
                          $sql = 'SELECT * FROM permissions';
                          $query = mysqli_query($conn,$sql);
                          mysqliDie($query);
                          while ($row = mysqli_fetch_assoc($query)) {
                            echo '<label class="col-sm-5"><input type="checkbox" '.checkPermission($row['perm_id'],itemInfo($conn,'users','user_id',$_GET['userid'],'permissions')).' value="'.$row['perm_id'].'" name="perm[]">'.$row['perm_name'].'</label>';
                          }
                        ?>
                      </div>             
                    </div>
                </div> 
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }else{ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <?php if(itemInfo($conn,'users','user_id',UserID(),'userlevel') == 1){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-suitcase"></i></span>
            <h3 class="panel-title">All Users</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=management&subpage=setup+users&useraction=add" class="pc-link-alt">Add New User</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Full Name</th>
                          <th>Username</th>
                          <th>User Role</th>
                          <th>Last Login</th>
                          <th>Date Added</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Full Name</th>
                          <th>Username</th>
                          <th>User Role</th>
                          <th>Last Login</th>
                          <th>Date Added</th>
                          <th>Actions</th>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php } ?>

<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/js/jasny.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-users.php'; ?>",
        stateSave: true
    });
    
  // Selectize
  $('.selectize').selectize();

  //
  $('#selectall').tCheckAll('.checkall');

});
</script>
<!--Include JS-->