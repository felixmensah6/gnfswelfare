<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/blockui/css/blockui.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'payall'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(UserID() == 1 && checkAccess('1',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-plus"></i></span>
            <h3 class="panel-title">Pay All - Funeral & Social</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=accounts&subpage=funeral+and+social" class="pc-link-alt">Manage Funeral & Social Account</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="" autocomplete="off">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Admin Password *</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" name="password" id="password" required placeholder="Admin Password Only" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="amount" id="amount" readonly value="<?php echo number_format((itemInfo($conn,'system_settings','id',5,'value') - ((itemInfo($conn,'system_settings','id',6,'value') / 100) * itemInfo($conn,'system_settings','id',5,'value'))),2); ?>" > 
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Period *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="period" id="period" data-date-format="dd/mm/yyyy" required placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="payall" id="payall">Pay All</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }elseif(isset($_GET['useraction']) && $_GET['useraction'] == 'add'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1,8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-plus"></i></span>
            <h3 class="panel-title">Add Funeral & Social Contribution</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=accounts&subpage=funeral+and+social" class="pc-link-alt">Manage Funeral & Social Account</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-social-account.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Member Name *</label>
                    <div class="col-sm-10">
                      <select name="account_holder" class="form-control selectize" placeholder="Select User Role" required>
                        <?php memberSelect($conn); ?>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Payment Type *</label>
                    <div class="col-sm-4">
                      <select name="payment_type" class="form-control selectize" placeholder="Select User Role" required>
                        <option value="1">Credit Account</option>
                        <option value="2">Debit Account</option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="amount" required placeholder="0.00" data-step="1" data-rule="currency">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Period *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="period" data-date-format="dd/mm/yyyy" required placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="payment_desc" placeholder="Description (optional)"></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }else{ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <?php if(checkAccess('8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-bank"></i></span>
            <h3 class="panel-title">Funeral & Social Account</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=accounts&subpage=funeral+and+social&useraction=payall" class="pc-link-alt">Pay All</a>
                <a href="?page=accounts&subpage=funeral+and+social&useraction=add" class="pc-link-alt">Pay Member</a>
                <?php } ?>
                
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>                          
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Balance</th>
                          <th>Duration</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>                          
                          <th>Debit</th>
                          <th>Credit</th>
                          <th>Balance</th>
                          <th>Duration</th>
                          <th>Actions</th>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php } ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/blockui/js/blockui.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-social.php'; ?>",
        stateSave: true
    });

  // Selectize
  $('.selectize').selectize();

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true,
    startView: 1
  });

  // clear out plugin default styling 
  $.blockUI.defaults.css = {};

  // Process Pay All
  $('#payall').on('click', function () {

    //form values
    var password = $("#password").val();
    var amount = $("#amount").val();
    var period = $("#period").val();
    var payall = $("#payall");
    var account = 2; //funeral & social account

    $.ajax({
        type: "POST",
        url: "<?php echo WORK_PATH . 'modify-payall.php'; ?>",
        data: 'password='+ password + '&amount=' + amount + '&period=' + period + '&account=' + account + '&payall=' + payall,
        beforeSend: function() {
          $.blockUI(); 
        },       
        success: function(option){
          $("#notification").html(option);

          //Hide alert
          setTimeout(function(){
            $('.alert').slideUp();
          },10000);

          $.unblockUI();
        }
    });
    return false;
  })

});
</script>
<!--Include JS-->