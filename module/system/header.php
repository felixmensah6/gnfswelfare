<!doctype html>
<html>
<head>
<meta http-equiv="refresh" content="<?php echo itemInfo($conn,'system_settings','id',2,'value'); ?>; url=<?php echo '?page=auth&subpage=logout'; ?>">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo APP_NAME; ?></title> 

<!--Include CSS-->
<link href="<?php echo CSS_PATH; ?>assets/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH; ?>assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH; ?>assets/css/default/external.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH; ?>assets/css/default/styles.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/alertify/css/alertify.css" rel="stylesheet" id="toggleCSS" type="text/css">
<!--Include CSS-->

<!--Include JS-->
<script src="<?php echo JS_PATH; ?>assets/js/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH; ?>assets/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH; ?>assets/js/default/stacksadmin.main.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/scrollbar/js/custom.scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/alertify/js/alertify.min.js" type="text/javascript"></script>
<!--Include JS-->

</head>

<body>

<!--Main Wrapper-->
<div class="wrapper">

  <!--Header-->
  <header class="header ps-fx">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
  
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo APP_URL; ?>">
            <img src="<?php echo IMG_PATH; ?>assets/images/logo.png" class="logo" alt="Logo"/>
            <?php echo itemInfo($conn,'system_settings','id',1,'value'); ?>
          </a>
          <button type="button" class="navbar-toggle navbar-toggle-left toggle-mobile">
            <span class="icon-bar"></span>
          </button>
          <ul class="navbar-mobile flt-r">
            <li><a href="#" class="toggle-quick-toolbar"><i class="icon-level-down"></i></a></li>
            <li><a href="#" class="collapsed" data-toggle="collapse" data-target="#navbar-top">
              <i class="icon-navicon"></i></a>
            </li>
          </ul>
        </div>
             
        <div class="collapse navbar-collapse" id="navbar-top">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" class="toggle-sidebar hidden-xs" title="Sidebar"><i class="icon-exchange"></i></a></li>
            <li><a href="#" class="toggle-fullscreen-on hidden-xs" title="Fullscreen"><i class="icon-arrows-alt"></i></a></li>
            <li><a href="#" class="toggle-quick-toolbar hidden-xs" title="Shortcut"><i class="icon-level-down"></i></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
                <img src="<?php echo CONT_PATH . ((itemInfo($conn,'users','user_id',UserID(),'avatar') == "") ? "images/avatar.png" : "images/users/" . itemInfo($conn,'users','user_id',UserID(),'avatar')); ?>" alt="avatar" class="img-circle">
                <?php echo ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname'))); ?> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu custom-dm" role="menu">
                <li><a href="?page=settings&subpage=edit+my+account&section=edit+profile"><i class="icon-cog"></i>Edit Account</a></li>
                <li><a href="#"><i class="icon-support"></i>Getting Started</a></li>
                <li><a href="#" data-toggle="modal" data-target=".about-software-modal"><i class="icon-question-circle"></i>About Software</a></li>
                <li><a href="?page=auth&subpage=logout"><i class="icon-power-off"></i>Logout</a></li>
              </ul>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
  </header>
  <!--Header-->
  
  <!--Sidebar-->
  <aside class="sidebar animated">
    <nav class="sidebar-nav">
      <ul class="sidebar-navbar">
        <li><a href="?page=dashboard&subpage=overview" <?php activeNav($_GET['subpage'],'overview'); ?>><i class="icon-home"></i> 
          <span>Dashboard</span>
          <span class="label label-info">Home</span></a>
        </li>
        <li class="heading"><h5>Membership</h5></li>
        <li><a href="#" <?php activeNav($_GET['page'],'members'); ?>><i class="icon-users"></i> <span>Members</span></a>
          <ul>

            <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
            <li><a href="?page=members&subpage=add+member" <?php activeNav($_GET['subpage'],'add member'); ?>><span>Add Member</span></a></li>
            <?php } ?>

            <li><a href="?page=members&subpage=manage+members" <?php activeNav($_GET['subpage'],'manage members'); ?>><span>Manage Members</span></a></li>
            <li><a href="?page=members&subpage=member+archives" <?php activeNav($_GET['subpage'],'member archives'); ?>><span>Member Archives</span></a></li>
          </ul>         
        </li>
        
        <?php if(checkAccess('1,8',$_SESSION['user_perms'])){ ?>
        <li class="heading"><h5>Finance</h5></li>

        <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
        <li><a href="?page=payment&subpage=make+payment" <?php activeNav($_GET['subpage'],'make payment'); ?>><i class="icon-money"></i> 
          <span>Make Payment</span></a>
        </li>
        <?php } ?>

        <?php if(checkAccess('8',$_SESSION['user_perms'])){ ?>
        <li><a href="#" <?php activeNav($_GET['page'],'accounts'); ?>><i class="icon-bank"></i> <span>Accounts</span></a>
          <ul>
            <li><a href="?page=accounts&subpage=welfare" <?php activeNav($_GET['subpage'],'welfare'); ?>><span>Welfare</span></a></li>
            <li><a href="?page=accounts&subpage=funeral+and+social" <?php activeNav($_GET['subpage'],'funeral and social'); ?>><span>Funeral & Social</span></a></li>
            <li><a href="?page=accounts&subpage=band" <?php activeNav($_GET['subpage'],'band'); ?>><span>Investment</span></a></li>
            <li><a href="?page=accounts&subpage=loans+and+recovery" <?php activeNav($_GET['subpage'],'loans and recovery'); ?>><span>Loans & Recovery</span></a></li>
            <li><a href="?page=accounts&subpage=over+deduction" <?php activeNav($_GET['subpage'],'over deduction'); ?>><span>Over Deduction</span></a></li>
          </ul>         
        </li>
        <?php } ?>
        <?php } ?>

        <?php if(checkAccess('1,8',$_SESSION['user_perms'])){ ?>
        <li><a href="#" <?php activeNav($_GET['page'],'expenses'); ?>><i class="icon-money"></i> <span>Expenses</span></a>
          <ul>

            <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
            <li><a href="?page=expenses&subpage=add+expenditure" <?php activeNav($_GET['subpage'],'add expenditure'); ?>><span>Add Expenditure</span></a></li>
            <?php } ?>

            <?php if(checkAccess('8',$_SESSION['user_perms'])){ ?>
            <li><a href="?page=expenses&subpage=manage+expenses" <?php activeNav($_GET['subpage'],'manage expenses'); ?>><span>Manage Expenses</span></a></li>
            <?php } ?>

          </ul>         
        </li>
        <?php } ?>

        <li class="heading"><h5>Administration</h5></li>
        <li><a href="#" <?php activeNav($_GET['page'],'management'); ?>><i class="icon-briefcase"></i> <span>Management</span></a>
          <ul>
            <li><a href="?page=management&subpage=asset+register" <?php activeNav($_GET['subpage'],'asset register'); ?>><span>Asset Register</span></a></li>

            <?php if(checkAccess('6',$_SESSION['user_perms'])){ ?>
            <li><a href="?page=management&subpage=setup+social+events" <?php activeNav($_GET['subpage'],'setup social events'); ?>><span>Setup Social Events</span></a></li>
            <li><a href="?page=management&subpage=setup+stations" <?php activeNav($_GET['subpage'],'setup stations'); ?>><span>Setup Stations</span></a></li>
            <li><a href="?page=management&subpage=setup+asset+category" <?php activeNav($_GET['subpage'],'setup asset category'); ?>><span>Setup Asset Category</span></a></li>
            <li><a href="?page=management&subpage=setup+expense+type" <?php activeNav($_GET['subpage'],'setup expense type'); ?>><span>Setup Expense Type</span></a></li>
            <?php } ?>

            <?php if(itemInfo($conn,'users','user_id',UserID(),'userlevel') == 1){ ?>
            <li><a href="?page=management&subpage=setup+users" <?php activeNav($_GET['subpage'],'setup users'); ?>><span>Setup Users</span></a></li>
            <?php } ?>

          </ul>         
        </li>
        <li class="heading"><h5>System</h5></li>
        <li><a href="#" <?php activeNav($_GET['page'],'settings'); ?>><i class="icon-cogs"></i> <span>Settings</span></a>
          <ul>

            <?php if(checkAccess('5',$_SESSION['user_perms'])){ ?>
            <li><a href="?page=settings&subpage=general+settings" <?php activeNav($_GET['subpage'],'general settings'); ?>><span>General Settings</span></a></li>
            <?php } ?>

            <li><a href="?page=settings&subpage=edit+my+account&section=edit+profile" <?php activeNav($_GET['subpage'],'edit my account'); ?>><span>Edit Account</span></a></li>
          </ul>         
        </li>

        <?php if(checkAccess('7',$_SESSION['user_perms'])){ ?>
        <li><a href="#" <?php activeNav($_GET['page'],'maintenance'); ?>><i class="icon-wrench"></i> <span>Maintenance</span></a>
          <ul>
            <li><a href="?page=maintenance&subpage=user+activities" <?php activeNav($_GET['subpage'],'user activities'); ?>><span>User Activities</span></a></li>
            <li><a href="?page=maintenance&subpage=backup+and+restore" <?php activeNav($_GET['subpage'],'backup and restore'); ?>><span>Backup & Restore</span></a></li>
            <li><a href="?page=maintenance&subpage=install+plugin" <?php activeNav($_GET['subpage'],'install plugin'); ?>><span>Install Plugin</span></a></li>
            <li><a href="?page=maintenance&subpage=system+update" <?php activeNav($_GET['subpage'],'system update'); ?>><span>System Update</span></a></li>
          </ul>         
        </li>
        <?php } ?>

      </ul>
    </nav>
  </aside>
  <!--Sidebar-->

  <!--Show Notification-->
  <div style="position: fixed; width: 100%; z-index: 10; top: 45px;" id="notification">
    <?php echo showNotification(); ?>
  </div>
  <!--Show Notification-->
  
  <!--Main Content-->
  <section class="content">
  
    <!--Quick Toolbar-->
    <div class="quick-toolbar animated">
      <div class="row m-0 toolbar-overlay">

        <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
        <div class="col-xs-6 col-sm-2 col-md-2 toolbar-list">
          <a href="?page=payment&subpage=make+payment">
            <i class="icon-money"></i> 
            <p>Make Payment</p>
          </a>
        </div>
        <?php } ?>
        
        <div class="col-xs-6 col-sm-2 col-md-2 toolbar-list">
          <a href="?page=members&subpage=manage+members" class="col-md-2">
            <i class="icon-users"></i> 
            <p>Members</p>
          </a>
        </div>

        <?php if(checkAccess('5',$_SESSION['user_perms'])){ ?>
        <div class="col-xs-6 col-sm-2 col-md-2 toolbar-list">
          <a href="?page=settings&subpage=general+settings">
            <i class="icon-cogs"></i> 
            <p>Settings</p>
          </a>
        </div>
        <?php } ?>

        <?php if(itemInfo($conn,'users','user_id',UserID(),'userlevel') == 1){ ?>
        <div class="col-xs-6 col-sm-2 col-md-2 toolbar-list">
          <a href="?page=management&subpage=setup+users">
            <i class="icon-user-plus"></i> 
            <p>Add User</p>
          </a>
        </div>
        <?php } ?>

        <?php if(checkAccess('7',$_SESSION['user_perms'])){ ?>
        <div class="col-xs-6 col-sm-2 col-md-2 toolbar-list">
          <a href="?page=maintenance&subpage=backup+and+restore">
            <i class="icon-database"></i> 
            <p>Backup</p>
          </a>
        </div>
        <?php } ?>

        <div class="col-xs-6 col-sm-2 col-md-2 toolbar-list">
          <a href="#">
            <i class="icon-question"></i> 
            <p>Help</p>
          </a>
        </div>
      </div>
    </div>
    <!--Quick Toolbar-->
    
    <!--Page Title Bar-->
    <div class="page-bar">
      <ol class="breadcrumb">
        <li><h3><?php echo ucwords(strtolower($_GET['page'])); ?></h3></li>
        <li><a href="<?php echo APP_URL; ?>"><i class="icon-home"></i></a></li>
        <li><a href="#"><?php echo ucwords(strtolower($_GET['page'])); ?></a></li>
        <li class="active"><?php echo ucwords(strtolower($_GET['subpage'])); ?></li>
      </ol>
      <div class="page-bar-tools">
        <input type="text" class="quick-search" id="keywords" name="keywords" placeholder="Search">
        <div id="results"></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <!--Page Title Bar-->
    