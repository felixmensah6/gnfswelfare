<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'add'){ ?>

    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-pencil"></i></span>
            <h3 class="panel-title">Add Asset</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=management&subpage=asset+register&useraction=add" class="pc-link-alt">Register Assets</a>
                <?php } ?>

                <a href="?page=management&subpage=asset+register" class="pc-link-alt">Manage Assets</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-asset.php'; ?>">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Tag/Reg. No *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="tag_no" placeholder="Tag / Registration No." required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Item Name *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="item_name" placeholder="Name of Item" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Quantity *</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="quantity" value="1" data-rule="quantity" data-min="1" placeholder="0" required autocomplete="off">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Serial No.</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="serial_no" placeholder="Serial No.">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Total Cost *</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="cost" data-rule="currency" placeholder="0.00" data-step="1" required autocomplete="off">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Category *</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="category" required>
                      <?php assetCategorySelect($conn); ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Purchase Date *</label>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" class="form-control datepicker" name="date" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" required>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Condition</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="condition" required>
                      <option value="1">In-Use</option>
                      <option value="2">Faulty</option>
                      <option value="3">Not In-Use</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="description" placeholder="Description (optional)"></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }elseif(isset($_GET['useraction']) && $_GET['useraction'] == 'edit'){ ?>

    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    
    <?php if(checkAccess('2',$_SESSION['user_perms'])){ ?><!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-pencil"></i></span>
            <h3 class="panel-title">Edit Asset</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=management&subpage=asset+register&useraction=add" class="pc-link-alt">Register Assets</a>
                <?php } ?>

                <a href="?page=management&subpage=asset+register" class="pc-link-alt">Manage Assets</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-asset.php?assetid='.$_GET['assetid']; ?>">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Tag/Reg. No *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="tag_no" placeholder="Tag / Registration No." required value="<?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'tag_no'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Item Name *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="item_name" placeholder="Name of Item" required value="<?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'item_name'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Quantity *</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="quantity" value="<?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'quantity'); ?>" data-rule="quantity" data-min="0" placeholder="0" required autocomplete="off">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Serial No.</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="serial_no" placeholder="Serial No." value="<?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'serial_no'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Total Cost *</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="cost" data-rule="currency" placeholder="0.00" required autocomplete="off" value="<?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'cost'); ?>">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Category *</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="category" required>
                      <?php assetCategorySelect($conn,itemInfo($conn,'asset_register','id',$_GET['assetid'],'category')); ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Purchase Date *</label>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <input type="text" class="form-control datepicker" name="date" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" required value="<?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'date'); ?>">
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Condition</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="condition" required>
                      <option value="1" <?php echo (itemInfo($conn,'asset_register','id',$_GET['assetid'],'status') == 1) ? "selected" : ""; ?>>In-Use</option>
                      <option value="2" <?php echo (itemInfo($conn,'asset_register','id',$_GET['assetid'],'status') == 2) ? "selected" : ""; ?>>Faulty</option>
                      <option value="3" <?php echo (itemInfo($conn,'asset_register','id',$_GET['assetid'],'status') == 3) ? "selected" : ""; ?>>Not In-Use</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="description" placeholder="Description (optional)"><?php echo itemInfo($conn,'asset_register','id',$_GET['assetid'],'description'); ?></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }else{ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-suitcase"></i></span>
            <h3 class="panel-title">All Assets</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=management&subpage=asset+register&useraction=add" class="pc-link-alt">Register Assets</a>
                <?php } ?>
                
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Tag No</th>
                          <th>Item Name</th>
                          <th>Quantity</th>
                          <th>Cost</th>
                          <th>Category</th>
                          <th>Condition</th>

                          <?php if(checkAccess('2,3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Tag No</th>
                          <th>Item Name</th>
                          <th>Quantity</th>
                          <th>Cost</th>
                          <th>Category</th>
                          <th>Condition</th>
                          
                          <?php if(checkAccess('2,3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

<?php } ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-assets.php'; ?>",
        stateSave: true
    });

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true
  });

  $('.selectize').selectize();



});
</script>
<!--Include JS-->