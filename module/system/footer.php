  </section>
  <!--Main Content-->

</div>
<!--Main Wrapper-->

<!-- Modal -->
<div class="modal fade about-software-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">About Software</h4>
      </div>
      <div class="modal-body" style="text-align: center;">
        <p><strong><?php echo APP_NAME; ?></strong></p>
        <p>Registered to</p>
        <p><?php echo APP_USER; ?></p>
        <p><?php echo 'Copyrite © '.date('Y',time()) .' <a href="http://'.APP_DEV_URL.'">'.APP_DEV.'</a> <br> '.APP_DEV_CON.' <br> Version '.APP_VER; ?></p>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

<!--Include JS-->
<script src="<?php echo JS_PATH; ?>assets/js/default/stacksadmin.scripts.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){

      //Alertify JS
      getID = function (id) {
        return document.getElementById(id);
      },
      reset = function () {
        getID("toggleCSS").href = "<?php echo HTML_PLUGIN_PATH . 'plugins/alertify/css/alertify.css'; ?>";
        alertify.set({
          labels: {
            ok: "Yes",
            cancel: "No"
          },
          delay: 5000,
          buttonReverse: false,
          buttonFocus: "ok"
        });
      };

});

	  function confirmDelete(value,url){
		//getID(value).onclick = function () {
		  reset();
		  alertify.confirm("Are you sure you want to continue?", function (e) {
			if (e) {
			  window.location = url;
			} else {
			  //alertify.error("You've clicked Cancel");
			}
		  });
		  return false;
		//};
	  };
      //Alertfy End

    //Hide alert
    setTimeout(function(){
      $('.alert').slideUp();
    },10000);

      //Search databse
    $("#keywords").keyup(function(){
      var kw = $("#keywords").val();
      var userID = "<?php echo UserID(); ?>";
      var dataString = 'kw='+ kw + '&userID=' + userID; 
      //alert(kw);
      if(kw != ''){ $.ajax({
         type: "POST",
         url: "<?php echo SYS_PATH . 'search-engine.php'; ?>",
         data: dataString,
         success: function(option){
          $("#results").html(option);
         }});}else{
       $("#results").html("");
       }
      return false;
    });
    
    $("#results").css('display','none');
     $("html").click(function(){
       $("#results").hide("#results").slideUp('fast');
     });
     
     $("#keywords").keyup(function(){
       $("#results").css('display','block');
       var max = 0;
       var len = $(this).val().length;
       if(len == max){
         $("#results").hide();
       }
     });
		
</script>
<!--Include JS-->

</body>
</html>