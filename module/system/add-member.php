<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/css/jasny.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-users"></i></span>
            <h3 class="panel-title">New Member Form</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=members&subpage=manage+members" class="pc-link-alt">Manage Memebers</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'create-members.php'; ?>" enctype="multipart/form-data">
                <div class="section-divider-blank"><span>Personal Information</span></div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Full Name *</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="firstname" required placeholder="First Name">
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="lastname" required placeholder="Last Name">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Date of Birth</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control datepicker" name="birthday" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                  </div>
                  <label for="input" class="col-sm-2 control-label">Gender *</label>
                  <div class="col-sm-4">
                    <label class="radio-inline">
                      <input type="radio" name="gender" required value="1" checked>Male
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="gender" required value="2">Female
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Marital Status</label>
                  <div class="col-sm-3">
                    <label class="radio-inline">
                      <input type="radio" name="marital_status" required value="1" checked>Single
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="marital_status" required value="2">Married
                    </label>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Name of Spouse</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="spouse" placeholder="Spouse Name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">No. of Children</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="children" value="0" data-rule="quantity" data-min="0">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Contact Number</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="input-contact" name="contact" placeholder="Contact Number">
                    <span class="help-block">Seperate numbers with a comma sign.</span>
                  </div>
                </div>
                <div class="section-divider-blank"><span>Other Information</span></div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Staff ID *</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" name="staff_id" required placeholder="Staff ID">
                  </div>
                  <label for="input" class="col-sm-2 control-label">Station *</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="station" required id="select-station">
                      <?php stationSelect($conn); ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Father's Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="father" placeholder="Father's Name">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Mother's Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="mother" placeholder="Mother's Name">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Postal Address</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="address" placeholder="Postal Address">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="email" placeholder="Email">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Upload Photo</label>
                  <div class="col-sm-5">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                      <div class="form-control" data-trigger="fileinput">
                        <span class="fa fa-file fileinput-exists"></span> 
                        <span class="fileinput-filename"></span>
                      </div>
                      <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select Image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="userfile">
                      </span>
                      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/js/jasny.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

  // Datepicker
  $('.datepicker').datepicker({
    startView: 2,
    autoclose: true
  });
    
  // Selectize
  $('#input-contact').selectize({
    persist: false,
    createOnBlur: true,
    create: true,
    maxItems: 3
  });

  $('#select-station').selectize();

  
});
</script>
<!--Include JS-->