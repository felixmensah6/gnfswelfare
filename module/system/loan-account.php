<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'add'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1,8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-plus"></i></span>
            <h3 class="panel-title">Grant Loan</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <?php if(isset($_GET['memberid'])){ ?>
                <a href="?page=members&subpage=manage+members&useraction=details&section=overview&memberid=<?php echo $_GET['memberid']; ?>" class="pc-link-alt dropdown-toggle"><i class="icon-arrow-circle-left"></i> Go Back</a>
                <?php } ?>
                <a href="?page=accounts&subpage=loans+and+recovery" class="pc-link-alt">Manage Loans</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span> 
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-loan.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Member Name *</label>
                    <div class="col-sm-10">
                      <select name="member_id" class="form-control selectize" placeholder="Select User Role" required>
                        <?php 
                          $memid = (isset($_GET['memberid'])) ? $_GET['memberid'] : "";
                          memberSelect($conn,$memid); 
                        ?>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="loan_amount" required placeholder="0.00" data-step="1" data-rule="currency">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Date *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="loan_date" data-date-format="dd/mm/yyyy" required placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Duration *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="duration" required placeholder="1" value="1" data-min="1" data-rule="quantity">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                      <span class="input-group-addon">
                        <label><input type="radio" name="duration_type" class="duration_type" value="1" checked> Months</label>
                        <label><input type="radio" name="duration_type" class="duration_type" value="2"> Years</label>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Interest Rate</label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <input type="text" class="form-control" name="rate" value="<?php echo itemInfo($conn,'system_settings','id',3,'value'); ?>" readonly>
                      <span class="input-group-addon">%</span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="loan_desc" placeholder="Description (optional)"></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }else{ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <?php if(checkAccess('8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-bank"></i></span>
            <h3 class="panel-title">Loans & Recovery Account</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=accounts&subpage=loans+and+recovery&useraction=add" class="pc-link-alt">Grant Loan</a>
                <?php } ?>
                
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>
                          <th>Loan Name</th>
                          <th>Status</th>
                          <th>Duration</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>
                          <th>Loan Name</th>
                          <th>Status</th>
                          <th>Duration</th>
                          <th>Actions</th>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php } ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-loan.php'; ?>",
        stateSave: true
    });

  // Selectize
  $('.selectize').selectize();

  <?php if(isset($_GET['memberid'])){ ?>
    $('.selectize')[0].selectize.lock();
  <?php } ?>

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true,
    startView: 1
  });

});
</script>
<!--Include JS-->