<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/css/jasny.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <?php if(checkAccess('5',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-cogs"></i></span>
            <h3 class="panel-title">System Configuration</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'update-general-settings.php'; ?>" enctype="multipart/form-data">
                <div class="section-divider-blank"><span>General Settings</span></div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Company Name *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="company_name" required placeholder="Company Name" value="<?php echo itemInfo($conn,'system_settings','id',1,'value'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Auto Logout *</label>
                    <div class="col-sm-5">
                      <select name="auto_logout" class="form-control selectize" required>
                        <option value="-1" <?php echo (itemInfo($conn,'system_settings','id',2,'value') == '-1') ? 'selected' : ''; ?>>Never Auto Logout</option>
                        <option value="600" <?php echo (itemInfo($conn,'system_settings','id',2,'value') == '600') ? 'selected' : ''; ?>>Auto Logout - 10 Minutes</option>
                        <option value="900" <?php echo (itemInfo($conn,'system_settings','id',2,'value') == '900') ? 'selected' : ''; ?>>Auto Logout - 15 Minutes</option>
                        <option value="1800" <?php echo (itemInfo($conn,'system_settings','id',2,'value') == '1800') ? 'selected' : ''; ?>>Auto Logout - 30 Minutes</option>
                        <option value="3600" <?php echo (itemInfo($conn,'system_settings','id',2,'value') == '3600') ? 'selected' : ''; ?>>Auto Logout - 1 Hour</option>
                      </select>
                      <span class="help-block">Logout when system is idle for the time specified</span>
                    </div>
                </div> 
                <div class="section-divider-blank"><span>Account Settings</span></div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Welfare *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="welfare_dues" placeholder="0.00" data-step="1" required value="<?php echo itemInfo($conn,'system_settings','id',4,'value'); ?>" data-rule="currency">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                    <span class="help-block">Monthly Welfare amount paid</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Funeral & Social *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="funeral_social" placeholder="0.00" data-step="1" required value="<?php echo itemInfo($conn,'system_settings','id',5,'value'); ?>" data-rule="currency">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                    <span class="help-block">Monthly F & S amount paid</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Controller Charge *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="controller_charge" placeholder="00.00" data-step="1" required value="<?php echo itemInfo($conn,'system_settings','id',6,'value'); ?>" data-rule="currency">
                      <span class="input-group-addon">%</span>
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                    <span class="help-block">Percentage deducted from dues</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Interest Rate *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <input type="text" class="form-control" name="interest" placeholder="00.00" data-step="1" required value="<?php echo itemInfo($conn,'system_settings','id',3,'value'); ?>" data-rule="currency">
                      <span class="input-group-addon">%</span>
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                    <span class="help-block">Interest rate paid on loans</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Currency Sign</label>
                    <div class="col-sm-4">
                      <select name="currency" class="form-control selectize">
                        <option value="GHS" <?php echo (itemInfo($conn,'system_settings','id',7,'value') == 'GHS') ? 'selected' : ''; ?>>Ghanaian New Cedi (GHS)</option>
                        <option value="GHC" <?php echo (itemInfo($conn,'system_settings','id',7,'value') == 'GHC') ? 'selected' : ''; ?>>Ghanaian Cedi (GHC)</option>
                        <option value="USD" <?php echo (itemInfo($conn,'system_settings','id',7,'value') == 'USD') ? 'selected' : ''; ?>>US Dollar (USD)</option>
                        <option value="GBP" <?php echo (itemInfo($conn,'system_settings','id',7,'value') == 'GBP') ? 'selected' : ''; ?>>British Pound (GBP)</option>
                      </select>
                    </div>
                </div> 
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/js/jasny.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

  $('.selectize').selectize();

  
});
</script>
<!--Include JS-->