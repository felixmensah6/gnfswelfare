<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<!--Include CSS-->


    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <?php if(checkAccess('7',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-wrench"></i></span>
            <h3 class="panel-title">All User Activities</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>User Name</th>
                          <th>Activity</th>
                          <th>Date</th>

                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>User Name</th>
                          <th>Activity</th>
                          <th>Date</th>
                          
                          <?php if(checkAccess('3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-logs.php'; ?>",
        stateSave: true
    });

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true
  });

  $('.selectize').selectize();



});
</script>
<!--Include JS-->