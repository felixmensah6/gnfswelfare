<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/css/jasny.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

    <!--Profile Box Start-->
    <div class="profile-header">
      <div class="row m-0">
        <div class="col-sm-3 p-0">
          <div class="profile-photo">
            <img src="<?php echo CONT_PATH . ((itemInfo($conn,'users','user_id',UserID(),'avatar') == "") ? "images/avatar.png" : "images/users/" . itemInfo($conn,'users','user_id',UserID(),'avatar')); ?>" alt="profile photo">
          </div>
        </div>
        <div class="col-sm-9 p-0">
          <div class="profile-details">
            <h2><?php echo ucwords(strtolower(itemInfo($conn,'users','user_id',UserID(),'firstname').' '.itemInfo($conn,'users','user_id',UserID(),'lastname'))); ?></h2>
            <p><strong>Username:</strong> <?php echo strtolower(itemInfo($conn,'users','user_id',UserID(),'username')); ?></p>
            <p><strong>Role:</strong> <?php echo userRole(itemInfo($conn,'users','user_id',UserID(),'userlevel')); ?></p>
            <p><strong>Date Added:</strong> <?php echo date('F Y',itemInfo($conn,'users','user_id',UserID(),'join_date')); ?></p>
            <p><strong>Last Login:</strong> <?php echo LastSeen(itemInfo($conn,'users','user_id',UserID(),'login_date')); ?></p>
          </div>
        </div>
      </div>
    </div>
    <!--Profile Box End-->

    <!--Page Nav Tab Start-->
    <ul class="nav nav-tabs page-nav-tab nav-right" role="tablist">
      <li <?php echo activeChildNav('edit profile',$_GET['section']); ?>><a href="javascript:;">Edit Profile</a></li>
    </ul>
    <!--Page Nav Tab End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <h3 class="panel-title">Edit Profile</h3>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <div class="section-divider-blank"><span>User Information</span></div>
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-account.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Full Name *</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="text" class="form-control"  name="firstname" required placeholder="First Name" value="<?php echo itemInfo($conn,'users','user_id',UserID(),'firstname'); ?>">
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control"  name="lastname" required placeholder="Last Name" value="<?php echo itemInfo($conn,'users','user_id',UserID(),'lastname'); ?>">
                    </div>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Upload Photo</label>
                  <div class="col-sm-5">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                      <div class="form-control" data-trigger="fileinput">
                        <span class="fa fa-file fileinput-exists"></span> 
                        <span class="fileinput-filename"></span>
                      </div>
                      <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select Image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="userfile">
                      </span>
                      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
              <div class="section-divider-blank"><span>Change Password</span></div>
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-account.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Old Password</label>
                    <div class="col-sm-5">
                      <input type="password" class="form-control"  name="password" required placeholder="Old Password">
                    </div>
                </div> 
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">New Password</label>
                  <div class="col-sm-10 p-0">
                    <div class="col-sm-6">
                      <input type="password" class="form-control"  name="password1" required placeholder="New Password">
                    </div>
                    <div class="col-sm-6">
                      <input type="password" class="form-control"  name="password2" required placeholder="Confirm New Password">
                    </div>
                  </div>
                </div> 
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="savepass">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/jasny/js/jasny.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    
  // Selectize
  $('.selectize').selectize();

});
</script>
<!--Include JS-->
