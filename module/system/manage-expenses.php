<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'edit'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-money"></i></span>
            <h3 class="panel-title">Edit Expenditure</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=expenses&subpage=add+expenditure" class="pc-link-alt">Add Expenditure</a>
                <a href="?page=expenses&subpage=manage+expenses" class="pc-link-alt">Manage Expenses</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'edit-expenditure.php?expid='.$_GET['expid']; ?>">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Expense Name *</label>
                  <div class="col-sm-5">
                    <select class="form-control selectize" name="exp_name" required>
                      <?php expenseTypeSelect($conn,itemInfo($conn,'expenditure','exp_id',$_GET['expid'],'exp_name')); ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-3">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="exp_amount" value="<?php echo number_format(itemInfo($conn,'expenditure','exp_id',$_GET['expid'],'exp_amount'),2); ?>" data-rule="currency" placeholder="0.00" required autocomplete="off">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Date</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control datepicker" name="exp_date" value="<?php echo dateFormat(itemInfo($conn,'expenditure','exp_id',$_GET['expid'],'exp_date'),'DD/MM/YYYY'); ?>" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy (optional)">
                    <span class="help-block">System date is used if left blank.</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="exp_desc" placeholder="Description (optional)"><?php echo itemInfo($conn,'expenditure','exp_id',$_GET['expid'],'exp_desc'); ?></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }else{ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-money"></i></span>
            <h3 class="panel-title">All Expenses</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=expenses&subpage=add+expenditure" class="pc-link-alt">Add Expenditure</a>
                <?php } ?>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Expense Name</th>
                          <th>Amount</th>
                          <th>Expense Date</th>
                          <th>Description</th>
                          <th>Date Added</th>

                          <?php if(checkAccess('2,3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Expense Name</th>
                          <th>Amount</th>
                          <th>Expense Date</th>
                          <th>Description</th>
                          <th>Date Added</th>
                          
                          <?php if(checkAccess('2,3',$_SESSION['user_perms'])){ ?>
                          <th>Actions</th>
                          <?php } ?>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->

<?php } ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-expenses.php'; ?>",
        stateSave: true
    });

  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true
  });

  $('.selectize').selectize();



});
</script>
<!--Include JS-->