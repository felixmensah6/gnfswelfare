
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-reorder"></i></span>
            <h3 class="panel-title">Database Backup</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.CLASS_PATH.'backup-database.php'; ?>" enctype="multipart/form-data">
                <div class="form-group">
                  <div class="pt-20">
                    <button type="submit" class="btn btn-success ml-10" name="save">Backup Database</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
