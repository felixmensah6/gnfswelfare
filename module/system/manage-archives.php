<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-users"></i></span>
            <h3 class="panel-title">Member Archives</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">
              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>
                          <th>Station</th>
                          <th>Gender</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Staff ID</th>
                          <th>Full Name</th>
                          <th>Station</th>
                          <th>Gender</th>
                          <th>Actions</th>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
  $('.basic-datatable').dataTable( {
      "ajax": "<?php echo WORK_PATH . 'process-member-archives.php'; ?>",
      stateSave: true
  });

});
</script>
<!--Include JS-->