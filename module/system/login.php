<?php 
//Check login status
if(!UserID()){
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo APP_NAME; ?></title>

<!--Include CSS-->
<link href="<?php echo CSS_PATH; ?>assets/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH; ?>assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH; ?>assets/css/default/external.css" rel="stylesheet" type="text/css">
<link href="<?php echo CSS_PATH; ?>assets/css/default/styles.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<!--Include JS-->
<script src="<?php echo JS_PATH; ?>assets/js/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH; ?>assets/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH; ?>assets/js/bootstrap/holder.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH; ?>assets/js/default/stacksadmin.main.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/scrollbar/js/custom.scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH; ?>assets/js/default/stacksadmin.scripts.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#login").click(function(){
        var username = $("#username").val();
          var password = $("#password").val();           
          var login = $("#login");
          var btn = $(this);
          btn.button('loading');
          var dataString = 'username='+ username + '&password=' + password + '&login=' + login; 
          //alert(kw);
          $.ajax({
               type: "POST",
               url: "module/workers/process-login.php",
               data: dataString,
               success: function(option){
                 $("#results").html(option);
                 btn.button('reset');
                 $(document).scrollTop(0);
               }});
          return false;
    });
  });
</script>
<!--Include JS-->

</head>

<body>

<!--Main Wrapper-->
<div class="wrapper min-h0" style="background: none;">

    <!--Content-->
    <div class="login-form">
      <div class="login-logo">
        <img src="<?php echo CONT_PATH; ?>images/logo2.png" alt="logo">
      </div>
      <form role="form">
        <div class="form-group">
          <div class="col-sm-12">
            <span id="results">
              <p>Staff Login</p>
            </span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="form-group">
          <div class="col-sm-12 prepend-icon">
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off">
            <span class="field-icon"><i class="icon-user"></i></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="form-group">
          <div class="col-sm-12 prepend-icon">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off">
            <span class="field-icon"><i class="icon-lock"></i></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" name="login" id="login" class="btn btn-success btn-block" data-loading-text="Wait..">Login</button> 
          </div>
          <div class="clearfix"></div>
        </div>
      </form>

      <footer class="login-footer">
        <p><?php echo date('Y',time()) .' © '.APP_NAME.' v'.APP_VER.'. <a href="http://'.APP_DEV_URL.'">'.APP_DEV.'</a>'; ?></p>
      </footer>
    </div>
    <!--Content-->

</div>
<!--Main Wrapper-->

</body>
</html>

<?php 
} else {
  header('Location: ' . DEFAULT_PAGE);
}
?>
