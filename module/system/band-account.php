<!--Include CSS-->
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/css/selectize.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/css/spinner.css" rel="stylesheet" type="text/css">
<!--Include CSS-->

<?php if(isset($_GET['useraction']) && $_GET['useraction'] == 'add'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('1,8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-plus"></i></span>
            <h3 class="panel-title">Add Investment</h3>
            <span class="panel-controls">
              <div class="btn-group">
                <a href="?page=accounts&subpage=band" class="pc-link-alt">Manage Investment Account</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">
              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-band-account.php'; ?>" enctype="multipart/form-data" autocomplete="off">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="amount_charged" required placeholder="0.00" data-step="1" data-rule="currency">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Date *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="eng_date" data-date-format="dd/mm/yyyy" required placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="eng_desc" placeholder="Description (optional)"></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <input type="hidden" name="expenses_made" value="0">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }elseif(isset($_GET['useraction']) == 'edit'){ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->

    <?php if(checkAccess('2,8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-pencil"></i></span>
            <h3 class="panel-title">Edit Investment</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=accounts&subpage=band&useraction=add" class="pc-link-alt">Add Investment</a>
                <?php } ?>

                <a href="?page=accounts&subpage=band" class="pc-link-alt">Manage Investment Account</a>
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body panel-form">

              <form class="form-horizontal" method="post" action="<?php echo APP_URL.WORK_PATH.'modify-band-account.php?engagementid='.$_GET['engagementid']; ?>" enctype="multipart/form-data" autocomplete="off">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Amount *</label>
                  <div class="col-sm-4">
                    <div class="input-group" data-trigger="spinner">
                      <span class="input-group-addon"><?php echo APP_CUR; ?></span>
                      <input type="text" class="form-control" name="amount_charged" required placeholder="0.00" data-rule="currency" value="<?php echo itemInfo($conn,'band_account','id',$_GET['engagementid'],'credit'); ?>">
                      <span class="input-group-addon">
                        <i class="icon-sort-asc spin-up" data-spin="up"></i>
                        <i class="icon-sort-desc spin-down" data-spin="down"></i>
                      </span>
                    </div>
                  </div>
                  <label for="input" class="col-sm-2 control-label">Date *</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="eng_date" data-date-format="dd/mm/yyyy" required placeholder="dd/mm/yyyy" value="<?php echo itemInfo($conn,'band_account','id',$_GET['engagementid'],'date'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="eng_desc" placeholder="Description (optional)"><?php echo itemInfo($conn,'band_account','id',$_GET['engagementid'],'description'); ?></textarea>
                    <div class="help-block sticky-help">
                      <strong>Hint:</strong> The system will generate a description if left blank.
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 pt-20">
                    <input type="hidden" name="expenses_made" value="0">
                    <button type="submit" class="btn btn-success ml-10" name="save">Save</button>
                  </div>
                </div> 
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php }else{ ?>
    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <?php if(checkAccess('8',$_SESSION['user_perms'])){ ?>
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="panel panel-light">
          <div class="panel-heading">
            <span class="panel-icon"><i class="icon-bank"></i></span>
            <h3 class="panel-title">Investment Account</h3>
            <span class="panel-controls">
              <div class="btn-group">

                <?php if(checkAccess('1',$_SESSION['user_perms'])){ ?>
                <a href="?page=accounts&subpage=band&useraction=add" class="pc-link-alt">Add Investment</a>
                <?php } ?>
               
              </div>
              <span class="divider"></span>
              <div class="btn-group">
                <a href="#" class="pc-link-alt toggle-panel"><i class="icon-minus"></i></a>
                <a href="#" class="pc-link-alt panel-screen-on"><i class="icon-expand"></i></a>
              </div>
            </span>
          </div>
          <div class="panel-collapse">
            <div class="panel-body">

              <table class="basic-datatable stripe" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Amount Charged</th>
                          <th>Expenses Made</th>
                          <th>Balance</th>
                          <th>Date</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
               
                  <tfoot>
                      <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Amount Charged</th>
                          <th>Expenses Made</th>
                          <th>Balance</th>
                          <th>Date</th>
                          <th>Actions</th>
                      </tr>
                  </tfoot>
               
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Row End-->
    <?php 
    }else{

        //include page
        include(SYS_PATH.'forbidden.php');

    }
    ?>

<?php } ?>


<!--Include JS-->
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/datatables/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/selectize/js/selectize.min.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/date-picker/js/datepicker.js" type="text/javascript"></script>
<script src="<?php echo HTML_PLUGIN_PATH; ?>plugins/spinner/js/spinner.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	// Basic Datatable
    $('.basic-datatable').dataTable( {
        "ajax": "<?php echo WORK_PATH . 'process-band-account.php'; ?>",
        stateSave: true
    });
    
  // Datepicker
  $('.datepicker').datepicker({
    autoclose: true
  });

});
</script>
<!--Include JS-->