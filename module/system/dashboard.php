    <!--Gap Start-->
    <div class="gap30"></div>
    <!--Gap End-->
    
    <!--Row Start-->
    <div class="row">
      <div class="col-sm-12 col-md-12">

        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-warning-light">
                <i class="icon-group pull-left"></i>
                <p class="number-stats-value"><?php echo countAll($conn,'members'); ?></p>
                <p class="number-stats-title">Members</p>
                <a href="?page=members&subpage=manage+members" class="btn-warning"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->

        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-primary-light">
                <i class="icon-dollar pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . @number_format(sumColumn($conn,'credit','welfare_account') - sumColumn($conn,'debit','welfare_account'),2); ?></p>
                <p class="number-stats-title">Welfare Dues</p>
                <a href="?page=accounts&subpage=welfare" class="btn-primary"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->   
        
        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-danger-light">
                <i class="icon-dollar pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . @number_format(sumColumn($conn,'credit','social_account') - sumColumn($conn,'debit','social_account'),2); ?></p>
                <p class="number-stats-title">Funeral & Social Dues</p>
                <a href="?page=accounts&subpage=funeral+and+social" class="btn-danger"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->
        
        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-success-light">
                <i class="icon-dollar pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . @number_format(sumColumn($conn,'credit','over_deduction_account') - sumColumn($conn,'debit','over_deduction_account'),2); ?></p>
                <p class="number-stats-title">Over Deductions</p>
                <a href="?page=accounts&subpage=over+deduction" class="btn-success"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->   
        
        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-info-light">
                <i class="icon-dollar pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . @number_format(sumColumn($conn,'credit','band_account') - sumColumn($conn,'debit','band_account'),2); ?></p>
                <p class="number-stats-title">Investments</p>
                <a href="?page=accounts&subpage=band" class="btn-info"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->
        
        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-dark-light">
                <i class="icon-bullhorn pull-left"></i>
                <p class="number-stats-value"><?php echo countAll($conn,'band_account'); ?></p>
                <p class="number-stats-title">Band Engagement</p>
                <a href="?page=accounts&subpage=band" class="btn-dark"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->  
        
        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-alert-light">
                <i class="icon-dollar pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . number_format(customQuery($conn,'SELECT SUM(debit) FROM loan_account WHERE credit < debit','SUM(debit)'),2); ?></p>
                <p class="number-stats-title">Loans</p>
                <a href="?page=accounts&subpage=loans+and+recovery" class="btn-alert"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->
        
        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-light-light">
                <i class="icon-money pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . number_format((customQuery($conn,'SELECT SUM(debit) FROM loan_account WHERE credit < debit','SUM(debit)') - customQuery($conn,'SELECT SUM(credit) FROM loan_account WHERE credit < debit','SUM(credit)')),2); ?></p>
                <p class="number-stats-title">Recovery Balance</p>
                <a href="?page=accounts&subpage=loans+and+recovery" class="btn-light"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats --> 

        <!-- Number Stats -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="number-stats bg-system-light">
                <i class="icon-dollar pull-left"></i>
                <p class="number-stats-value"><?php echo APP_CUR . number_format(customQuery($conn,'SELECT SUM(credit) FROM loan_account WHERE credit < debit','SUM(credit)'),2); ?></p>
                <p class="number-stats-title">Loans Recovered</p>
                <a href="?page=accounts&subpage=loans+and+recovery" class="btn-system"> VIEW MORE <i class="icon-angle-right pull-right"></i></a>
            </div>
        </div>
        <!-- Number Stats -->   

      </div>
    </div>
    <!--Row End-->