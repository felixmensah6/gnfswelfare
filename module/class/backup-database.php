<?php
//start output buffering
ob_start();

//include the configuration and functions
require_once('../include/session.php');
require_once('../include/class.php');
require_once('../include/config.php');
require_once('../include/function.php');
require_once('../include/language.php');

	backup_database_tables('localhost','smngh_gnfs','af0209493590','smngh_welfare','*');
	 
	// backup the db function
	function backup_database_tables($host,$user,$pass,$name,$tables)
	{
	 
	    $link = mysqli_connect($host,$user,$pass);
	    mysqli_select_db($link,$name);
	 
	    //get all of the tables
	    if($tables == '*')
	    {
	        $tables = array();
	        $result = mysqli_query($link,'SHOW TABLES');
	        while($row = mysqli_fetch_row($result))
	        {
	            $tables[] = $row[0];
	        }
	    }
	    else
	    {
	        $tables = is_array($tables) ? $tables : explode(',',$tables);
	    }
	 
	    //cycle through each table and format the data
		$return = '';
	    foreach($tables as $table)
	    {
	        $result = mysqli_query($link,'SELECT * FROM '.$table);
	        $num_fields = mysqli_num_fields($result);
	 
	        $return .= '/* GNFS Eastern Regional Welfare Database */;';
	        $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
	        $return.= "\n\n".$row2[1].";\n\n";
	 
	        for ($i = 0; $i < $num_fields; $i++)
	        {
	            while($row = mysqli_fetch_row($result))
	            {
	                $return.= 'INSERT INTO '.$table.' VALUES(';
	                for($j=0; $j<$num_fields; $j++)
	                {
	                    $row[$j] = addslashes($row[$j]);
	                    $row[$j] = str_replace("\n","\\n",$row[$j]);
	                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
	                    if ($j<($num_fields-1)) { $return.= ','; }
	                }
	                $return.= ");\n";
	            }
	        }
	        $return.="\n\n\n";
	    }
	 
	    //save the file
	    $handle = fopen($dirname . '../../backups/db-backup-'.date('d-m-Y').'.sql','w+');
	    fwrite($handle,$return);
		
		/* $time = time();
		$filename = 'db-backup-'.date('d-m-Y').'.sql';
		
		//insert into database with new data if everything is OK!
		$sql = "INSERT INTO db_backups (backup_time, backup_file) VALUES ('".$time."','".$filename."')";
		$results = mysql_query( $sql);
		if(!$result){
	 	 die('Error: ' . mysqli_error($conn));
		} */
		  
	    fclose($handle);

	    setNotification(1,'Database backup complete');
	}

//flush output buffering
ob_end_flush(); 
?>