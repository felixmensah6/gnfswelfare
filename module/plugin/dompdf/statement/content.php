<?php
//start output buffering
ob_start();

//include the configuration and functions
require_once('../../../include/session.php');
require_once('../../../include/dbconnect.php');
require_once('../../../include/class.php');
require_once('../../../include/config.php');
require_once('../../../include/function.php');
require_once('../../../include/language.php');

//
$memberid = clean($conn,$_GET['memberid']);
$userid = clean($conn,$_GET['userid']);
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Account Statement</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="footer">
  <div class="page-number"></div>
</div>

<div style="text-align: center;">
	<img src="logo.png" width="100px" height="100px" />
	<h4>GHANA NATIONAL FIRE SERVICE</h4>
	<h5>EASTERN REGIONAL WELFARE FUND</h5>
  <h5>P. O. BOX 247, KOFORIDUA</h5>
  <h5>0207389468 | ergnfswelfarefund@gmail.com</h5>
	<h5 class="blue" style="margin-top: 20px;">ACCOUNT STATEMENT</h5>
	<div class="hr"></div>
  <div class="passport-photo" style="top: 90px; right: 10px;"">
    <?php
    if(itemInfo($conn,'members','member_id',$_GET['memberid'],'photo') != ""){
        echo '<img src="';
        echo APP_URL . CONT_PATH . 'images/members/' . itemInfo($conn,'members','member_id',$_GET['memberid'],'photo');
        echo '" alt="" width="100%"/>';
    }else{
        echo '';
    }
    ?>
  </div>
</div>

<div style="font-size: 11px;">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td colspan="5">
            <p class="blue"><strong>Reference</strong></p>
          </td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Name:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo ucwords(strtolower(itemInfo($conn,'members','member_id',$_GET['memberid'],'firstname').' '.itemInfo($conn,'members','member_id',$_GET['memberid'],'lastname'))); ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Date:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo date('d-M-Y'); ?></span></p></td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Staff ID:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo itemInfo($conn,'members','member_id',$_GET['memberid'],'staff_id'); ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Period:</span></p></td>
          <td width="31%" align="right"><p><span>All</span></p></td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Station:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo ucwords(strtolower(itemInfo($conn,'stations','station_id',itemInfo($conn,'members','member_id',$_GET['memberid'],'station'),'station_name'))); ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Issued By:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo ucwords(strtolower(itemInfo($conn,'users','user_id',$_GET['userid'],'firstname').' '.itemInfo($conn,'users','user_id',$_GET['userid'],'lastname'))); ?></span></p></td>
        </tr>
      </tbody>
    </table>

  <div class="hr-dotted"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td colspan="5">
            <p class="blue"><strong>Summary</strong></p>
          </td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Welfare:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format(itemInfo($conn,'welfare_account','member_id',$_GET['memberid'],'credit') - itemInfo($conn,'welfare_account','member_id',$_GET['memberid'],'debit'),2); ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Funeral & Social:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format(itemInfo($conn,'social_account','member_id',$_GET['memberid'],'credit') - itemInfo($conn,'social_account','member_id',$_GET['memberid'],'debit'),2); ?></span></p></td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Over Deductions:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format(itemInfo($conn,'over_deduction_account','member_id',$_GET['memberid'],'credit') - itemInfo($conn,'over_deduction_account','member_id',$_GET['memberid'],'debit'),2); ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Funded Events:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format(sumSpecificColumn($conn,'amount_given','social_event','member_id',$_GET['memberid']),2) ; ?></span></p></td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Loans Acquired:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format(customQuery($conn,"SELECT SUM(debit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(debit)"),2) ; ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Loans Recovered:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format(customQuery($conn,"SELECT SUM(credit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(credit)"),2) ; ?></span></p></td>
        </tr>
        <tr>
          <td width="17%" align="left"><p><span class="blue">Loan Arrears:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo number_format((customQuery($conn,"SELECT SUM(debit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(debit)") - customQuery($conn,"SELECT SUM(credit) FROM loan_account WHERE member_id=".$_GET['memberid']." AND credit < debit","SUM(credit)")),2) ; ?></span></p></td>
          <td width="6%" align="center"></td>
          <td width="17%" align="left"><p><span class="blue">Currency:</span></p></td>
          <td width="31%" align="right"><p><span><?php echo itemInfo($conn,'system_settings','id',7,'value'); ?></span></p></td>
        </tr>
      </tbody>
    </table>

	<div class="hr"></div>

  <div style="text-align: center;">
    <h4 class="blue" style="margin-top: 40px;">WELFARE CONTRIBUTIONS</h4>
  </div>

  <div style="height: 5px;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <td><p><span class="blue"><strong>Trans. Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Description</strong></span></p></td>
          <td><p><span class="blue"><strong>Value Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Debit</strong></span></p></td>
          <td><p><span class="blue"><strong>Credit</strong></span></p></td>
        </tr>
      </thead>
      <tbody>
      <?php
        //show records
        $sql = "SELECT * FROM welfare_details WHERE member_id=".$memberid."";
        $query = mysqli_query($conn,$sql);
        mysqliDie($conn,$query);
        $aff_rows = mysqli_num_rows($query);
        if($aff_rows == 0){
          echo '<tr><td  colspan="20">No records available!</td></tr>';
        }else{
          while ($row = mysqli_fetch_assoc($query)) {
      ?>
        <tr>
          <td><p><span><?php echo date('d-M-Y',$row['time']); ?></span></p></td>
          <td><p><span><?php echo $row['description']; ?></span></p></td>
          <td><p><span><?php echo dateFormat($row['period'],'DD-MMM-YYYY',1); ?></span></p></td>
          <td><p><span><?php echo number_format($row['debit'],2); ?></span></p></td>
          <td><p><span><?php echo number_format($row['credit'],2); ?></span></p></td>
        </tr>
      <?php
          }
        }
      ?>
      </tbody>
    </table>

  <div style="text-align: center;">
    <h4 class="blue" style="margin-top: 40px;">FUNERAL & SOCIAL CONTRIBUTIONS</h4>
  </div>

  <div style="height: 5px;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <td><p><span class="blue"><strong>Trans. Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Description</strong></span></p></td>
          <td><p><span class="blue"><strong>Value Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Debit</strong></span></p></td>
          <td><p><span class="blue"><strong>Credit</strong></span></p></td>
        </tr>
      </thead>
      <tbody>
      <?php
        //show records
        $sql = "SELECT * FROM social_details WHERE member_id=".$memberid."";
        $query = mysqli_query($conn,$sql);
        mysqliDie($conn,$query);
        $aff_rows = mysqli_num_rows($query);
        if($aff_rows == 0){
          echo '<tr><td  colspan="20">No records available!</td></tr>';
        }else{
          while ($row = mysqli_fetch_assoc($query)) {
      ?>
        <tr>
          <td><p><span><?php echo date('d-M-Y',$row['time']); ?></span></p></td>
          <td><p><span><?php echo $row['description']; ?></span></p></td>
          <td><p><span><?php echo dateFormat($row['period'],'DD-MMM-YYYY',1); ?></span></p></td>
          <td><p><span><?php echo number_format($row['debit'],2); ?></span></p></td>
          <td><p><span><?php echo number_format($row['credit'],2); ?></span></p></td>
        </tr>
      <?php
          }
        }
      ?>
      </tbody>
    </table>

  <div style="text-align: center;">
    <h4 class="blue" style="margin-top: 40px;">OVER DEDUCTIONS</h4>
  </div>

  <div style="height: 5px;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <td><p><span class="blue"><strong>Trans. Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Description</strong></span></p></td>
          <td><p><span class="blue"><strong>Value Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Debit</strong></span></p></td>
          <td><p><span class="blue"><strong>Credit</strong></span></p></td>
        </tr>
      </thead>
      <tbody>
      <?php
        //show records
        $sql = "SELECT * FROM over_deduction_details WHERE member_id=".$memberid."";
        $query = mysqli_query($conn,$sql);
        mysqliDie($conn,$query);
        $aff_rows = mysqli_num_rows($query);
        if($aff_rows == 0){
          echo '<tr><td  colspan="20">No records available!</td></tr>';
        }else{
          while ($row = mysqli_fetch_assoc($query)) {
      ?>
        <tr>
          <td><p><span><?php echo date('d-M-Y',$row['time']); ?></span></p></td>
          <td><p><span><?php echo $row['description']; ?></span></p></td>
          <td><p><span><?php echo dateFormat($row['period'],'DD-MMM-YYYY',1); ?></span></p></td>
          <td><p><span><?php echo number_format($row['debit'],2); ?></span></p></td>
          <td><p><span><?php echo number_format($row['credit'],2); ?></span></p></td>
        </tr>
      <?php
          }
        }
      ?>
      </tbody>
    </table>

  <div style="text-align: center;">
    <h4 class="blue" style="margin-top: 40px;">LOANS ACQUIRED</h4>
  </div>

  <div style="height: 5px;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <td><p><span class="blue"><strong>Trans. Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Description</strong></span></p></td>
          <td><p><span class="blue"><strong>Amount</strong></span></p></td>
          <td><p><span class="blue"><strong>Duration</strong></span></p></td>
          <td><p><span class="blue"><strong>Status</strong></span></p></td>
        </tr>
      </thead>
      <tbody>
      <?php
        //show records
        $sql = "SELECT * FROM loan_account WHERE member_id=".$memberid."";
        $query = mysqli_query($conn,$sql);
        mysqliDie($conn,$query);
        $aff_rows = mysqli_num_rows($query);
        if($aff_rows == 0){
          echo '<tr><td  colspan="20">No records available!</td></tr>';
        }else{
          while ($row = mysqli_fetch_assoc($query)) {
      ?>
        <tr>
          <td><p><span><?php echo dateFormat($row['date'],'DD-MMM-YYYY',1); ?></span></p></td>
          <td><p><span><?php echo $row['payment_name']; ?></span></p></td>
          <td><p><span><?php echo number_format($row['debit'],2); ?></span></p></td>
          <td><p><span><?php echo paymentDuration($row['duration']); ?></span></p></td>
          <td><p><span><?php echo loanStatus($row['credit'] - $row['debit']); ?></span></p></td>
        </tr>
      <?php
          }
        }
      ?>
      </tbody>
    </table>

  <div style="text-align: center;">
    <h4 class="blue" style="margin-top: 40px;">LOAN RECOVERY DETAILS</h4>
  </div>

  <div style="height: 5px;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <td><p><span class="blue"><strong>Trans. Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Description</strong></span></p></td>
          <td><p><span class="blue"><strong>Amount</strong></span></p></td>
          <td><p><span class="blue"><strong>Period</strong></span></p></td>
          <td><p><span class="blue"><strong>Duration Left</strong></span></p></td>
        </tr>
      </thead>
      <tbody>
      <?php
        //show records
        $sql = "SELECT * FROM loan_recovery WHERE member_id=".$memberid."";
        $query = mysqli_query($conn,$sql);
        mysqliDie($conn,$query);
        $aff_rows = mysqli_num_rows($query);
        if($aff_rows == 0){
          echo '<tr><td  colspan="20">No records available!</td></tr>';
        }else{
          while ($row = mysqli_fetch_assoc($query)) {
      ?>
        <tr>
          <td><p><span><?php echo dateFormat($row['date'],'DD-MMM-YYYY',1); ?></span></p></td>
          <td><p><span><?php echo $row['payment_name']; ?></span></p></td>
          <td><p><span><?php echo number_format($row['credit'],2); ?></span></p></td>
          <td><p><span><?php echo getPeriod($row['period']); ?></span></p></td>
          <td><p><span><?php echo paymentDuration($row['duration_left']); ?></span></p></td>
        </tr>
      <?php
          }
        }
      ?>
      </tbody>
    </table>

  <div style="text-align: center;">
    <h4 class="blue" style="margin-top: 40px;">FUNERAL & SOCIAL ACTIVITIES</h4>
  </div>

  <div style="height: 5px;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <td><p><span class="blue"><strong>Trans. Date</strong></span></p></td>
          <td><p><span class="blue"><strong>Description</strong></span></p></td>
          <td><p><span class="blue"><strong>Amount</strong></span></p></td>
          <td><p><span class="blue"><strong>Event Date</strong></span></p></td>
        </tr>
      </thead>
      <tbody>
      <?php
        //show records
        $sql = "SELECT * FROM social_event WHERE member_id=".$memberid."";
        $query = mysqli_query($conn,$sql);
        mysqliDie($conn,$query);
        $aff_rows = mysqli_num_rows($query);
        if($aff_rows == 0){
          echo '<tr><td  colspan="20">No records available!</td></tr>';
        }else{
          while ($row = mysqli_fetch_assoc($query)) {
      ?>
        <tr>
          <td><p><span><?php echo date('d-M-Y',$row['event_time']); ?></span></p></td>
          <td><p><span><?php echo $row['event_name']; ?></span></p></td>
          <td><p><span><?php echo number_format($row['amount_given'],2); ?></span></p></td>
          <td><p><span><?php echo dateFormat($row['event_date'],'DD-MMM-YYYY',1); ?></span></p></td>
        </tr>
      <?php
          }
        }
      ?>
      </tbody>
    </table>

    <div style="height: 30px;"></div>

    <div class="hr"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td style="font-size: 10px;text-align: center;">WELFARE MANAGEMENT SYSTEM. By Unity Websoft. &copy; 2015. Tel: 0266666605, 0266666612 Email: unitywebsoft@gmail.com</td>
      </tr>
    </table>
</div>

</body>
</html>

<?PHP
//flush output buffering
ob_end_flush();
?>
