<?php
/***************************
 SYSTEM CONFIGURATION
***************************/

	// Application Name
	define('APP_NAME','Welfare Management System');

	// Application Version
	define('APP_VER','1.0');

	// Application URL
	define('APP_URL','http://'.$_SERVER['HTTP_HOST'].'/');

	// Application Developer
	define('APP_DEV','Unity Websoft');

	// Application Developer URL
	define('APP_DEV_URL','www.unitywebsoft.com');

	// Application Developer Contact
	define('APP_DEV_CON','026 6666 612 / 026 6666 605');

	// Application User
	define('APP_USER','Ghana National Fire Service <br> <em>Asamankese Welfare</em>');

	// Default page
	define('DEFAULT_PAGE','?page=dashboard&subpage=overview');

	// Error page
	define('ERROR_PAGE','?page=dashboard&subpage=overview');

	// Loan Recovery Top-Up
	define('LOAN_TOPUP','0.0');

	// Currency
	(isset($_SESSION['app_currency'])) ? $app_cur = $_SESSION['app_currency'] : $app_cur = '';
	define('APP_CUR', $app_cur.' ');

/***************************
 PATH CONFIGURATION
***************************/

	// CSS Path
	define('CSS_PATH','');

	// JS Path
	define('JS_PATH','');

	// HTML Plugin Path
	define('HTML_PLUGIN_PATH','');

	// Image Path
	define('IMG_PATH','');

	// Plugin Path
	define('PLUGIN_PATH','module/plugin/');

	// System Path
	define('SYS_PATH','module/system/');

	// Class Path
	define('CLASS_PATH','module/class/');

	// Content Path
	define('CONT_PATH','module/content/');

	// Workers Path
	define('WORK_PATH','module/workers/');


/***************************
 ERROR REPORTING
***************************/

	//Turn display errors on (1) or off (0)
	ini_set('display_errors',0);
?>
