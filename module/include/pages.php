<?php

$getPages = array(

	/********************************
	 SYSTEM PAGES
	********************************/
	'overview' 					=> 'module/system/dashboard.php',
	'manage members' 			=> 'module/system/manage-members.php',
	'member archives' 			=> 'module/system/manage-archives.php',
	'add member' 				=> 'module/system/add-member.php',
	'make payment' 				=> 'module/system/make-payment.php',
	'welfare' 					=> 'module/system/welfare-account.php',
	'funeral and social' 		=> 'module/system/social-account.php',
	'band' 						=> 'module/system/band-account.php',
	'loans and recovery' 		=> 'module/system/loan-account.php',
	'over deduction' 			=> 'module/system/over-deduction-account.php',
	'add expenditure' 			=> 'module/system/add-expenditure.php',
	'manage expenses' 			=> 'module/system/manage-expenses.php',
	'asset register' 			=> 'module/system/asset-register.php',
	'setup social events' 		=> 'module/system/setup-social-events.php',
	'setup stations' 			=> 'module/system/setup-stations.php',
	'setup asset category' 		=> 'module/system/setup-asset-category.php',
	'setup expense type' 		=> 'module/system/setup-expense-type.php',
	'setup users' 				=> 'module/system/setup-users.php',
	'general settings' 			=> 'module/system/general-settings.php',
	'edit my account' 			=> 'module/system/edit-account.php',
	'user activities' 			=> 'module/system/user-activities.php',
	'backup and restore' 		=> 'module/system/backup-and-restore.php',
	'install plugin' 			=> 'module/system/install-plugin.php',
	'system update' 			=> 'module/system/system-update.php',
	'logout' 					=> 'module/system/logout.php',
	'archive' 					=> 'module/system/archiver.php'

);
?>