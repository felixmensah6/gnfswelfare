<?php

$lang = array(

	/********************************
	 NOTIFICATION LANGUAGE
	********************************/
	'blank_input_error' 			=> 'All fields marked * are required. Please check your inputs and try again',
	'is_numeric_error' 				=> 'The amount must be a decimal number e.g. GH&cent; 20.00 . Please check your input',
	'interest_rate_error' 			=> 'The Interest Rate must be a decimal number e.g. 10.50% . Please check your input',
	'no_amount_error' 				=> 'You did not enter an amount',
	'spouse_name_error' 			=> 'Please provide the name of your Spouse',
	'email_error' 					=> 'The email address you entered is invalid. e.g example@site.com',
	'filesize_error' 				=> 'The maximum file size allowed is ',
	'image_dimension_error' 		=> 'Your image width should be at least 500px and height 500px',
	'folder_write_error' 			=> ' We were unable to access your upload folder.Please contact Admin',
	'image_ext_error' 				=> 'You can only upload jpg, gif and png images',
	'member_exist_error' 			=> 'There is a Member with the same Staff ID. Please check and try again',
	'user_exist_error' 				=> 'The username you entered is not available. Please try another',
	'username_length_error' 		=> 'The username must be at least ',
	'username_invalid_error' 		=> 'Only alphanumeric characters allowed. e.g john09 or john',
	'password_match_error' 			=> 'The password you entered do not match',
	'password_length_error' 		=> 'The password must be at least ',
	'old_password_error' 			=> 'The old password you entered is wrong. Please check and try again',
	'station_code_error' 			=> 'Your Station Code is invalid. Please make sure you have entered a number',
	'update_file_error' 			=> 'Please select a valid update file',
	'update_filetype_error' 		=> 'You can only upload zip files',
	'install_update_error' 			=> 'Unable to install update',
	'no_credit_error' 				=> 'There were no funds to debit',
	'grant_loan_error' 				=> 'You cannot grant a loan through this option',
	'no_loan_error' 				=> 'The selected Member did not acquire for loan',
	'no_permission_error' 			=> 'You must select at least one permission',
	'wrong_password_error' 			=> 'You entered a wrong administrative password',
	'wrong_perm_error' 				=> 'You do not have permission to carry out this action',
	'general_error' 				=> 'Unable to complete command. Contact Admin for help',
	'install_update_success' 		=> 'System update complete',
	'save_success' 					=> 'New record saved',
	'update_success' 				=> 'Your changes have been saved',
	'delete_success' 				=> 'Record deleted'

);
?>