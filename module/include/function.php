<?php
//thee user id from session variable
function UserID(){
	if(isset($_SESSION['user_id'])){
		return $_SESSION['user_id'];
	}
}

//grant or deny access if true
function checkAccess($needle,$haystack){
	$key = explode(',',$needle);
	$value = explode(',',$haystack);
	if(array_intersect($key, $value)){
		return true;
	}else{
		return false;
	}
}

//MySQLi die function
function mysqliDie($result){
	global $conn;
	if(!$result)
	{
 	 die('Error: ' . mysqli_error($conn));
	}
}

//url encoding functions returned
function encodeURL($string){
	return urlencode($string);
}

//sanitize db data
function clean($conn,$str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return mysqli_real_escape_string($conn,$str);
}

//select item details from db
function itemInfo($conn,$table,$where,$rowid,$column){
	$sql = "SELECT * FROM ".$table." WHERE ".$where."='".$rowid."'";
	$iteminfo = mysqli_query($conn,$sql);
	mysqliDie($iteminfo);
	$iteminforow = mysqli_fetch_array($iteminfo);
	return $iteminforow[$column];
}

//update item details from the database
function itemUpdate($conn,$table,$where,$rowid,$column,$value){
	$sql = "UPDATE ".$table." SET ".$column."='".$value."' WHERE ".$where."='".$rowid."'";
	$itemupdate = mysqli_query($conn,$sql);
	mysqliDie($itemupdate);
}

//active nav selector
function activeNav($url_value='',$page){
	if($url_value == $page){
		echo 'class="active"';
	}else{
		echo '';
	}
}

//active child nav selector
function activeChildNav($url_value='',$db_value){
	if($url_value == $db_value){
		return 'class="active"';
	}else{
		return '';
	}
}

//fetch pages
function fetchPages($pages_array,$default_page){
	global $conn;
	if(isset($_GET['subpage'])){
		if(array_key_exists($_GET['subpage'],$pages_array)){
			require($pages_array[$_GET['subpage']]);
		}else{
			require('module/system/dashboard.php');
		}
	}else{
		header('Location: ' . $default_page);
	}
}

//select list of stations
function stationSelect($conn,$value = ''){
	$sql = 'SELECT * FROM stations ORDER BY station_code ASC';
	$select = mysqli_query($conn, $sql);
	mysqliDie($select);
		echo '<option value="">Select Station</option>';
	while($row = mysqli_fetch_array($select)) {
		($value == $row['station_id']) ? $act = 'selected' : $act = '';
		echo '<option '.$act.' value="'.$row['station_id'].'">'.$row['station_code'].' - '.ucwords(strtolower($row['station_name'])).'</option>';
	}
}

//select list of expense types
function expenseTypeSelect($conn,$value = ''){
	$sql = 'SELECT * FROM expense_type';
	$select = mysqli_query($conn, $sql);
	mysqliDie($select);
		echo '<option value="">Select Expense Name</option>';
	while($row = mysqli_fetch_array($select)) {
		($value == $row['id']) ? $act = 'selected' : $act = '';
		echo '<option '.$act.' value="'.$row['id'].'">'.ucwords(strtolower($row['expense'])).'</option>';
	}
}

//select list of social events
function eventNameSelect($conn,$value = ''){
	$sql = 'SELECT * FROM event_type';
	$select = mysqli_query($conn, $sql);
	mysqliDie($select);
		echo '<option value="">Select Event</option>';
	while($row = mysqli_fetch_array($select)) {
		($value == $row['id']) ? $act = 'selected' : $act = '';
		echo '<option '.$act.' value="'.$row['event'].'">'.ucwords(strtolower($row['event'])).'</option>';
	}
}

//select list of asset categories
function assetCategorySelect($conn,$value = ''){
	$sql = 'SELECT * FROM asset_category';
	$select = mysqli_query($conn, $sql);
	mysqliDie($select);
		echo '<option value="">Select Category</option>';
	while($row = mysqli_fetch_array($select)) {
		($value == $row['id']) ? $act = 'selected' : $act = '';
		echo '<option '.$act.' value="'.$row['id'].'">'.ucwords(strtolower($row['category'])).'</option>';
	}
}

//select list of user roles
function userRoleSelect($conn,$value = ''){
	$sql = 'SELECT * FROM user_roles';
	$select = mysqli_query($conn, $sql);
	mysqliDie($select);
		echo '<option value="">Select User Roles</option>';
	while($row = mysqli_fetch_array($select)) {
		($value == $row['role_number']) ? $act = 'selected' : $act = '';
		echo '<option '.$act.' value="'.$row['role_number'].'">'.ucwords(strtolower($row['role_name'])).'</option>';
	}
}

//select list of members
function memberSelect($conn,$value = ''){
	$sql = 'SELECT * FROM members WHERE status=1 ORDER BY staff_id';
	$select = mysqli_query($conn, $sql);
	mysqliDie($select);
		echo '<option value="">Select A Member</option>';
	while($row = mysqli_fetch_array($select)) {
		($value == $row['member_id']) ? $act = 'selected' : $act = '';
		echo '<option '.$act.' value="'.$row['member_id'].'">'.userTitle($row['gender']).' '.ucwords(strtolower($row['firstname'])).' '.ucwords(strtolower($row['lastname'])).' - '.$row['staff_id'].'</option>';
	}
}

//sum selected column from db
function sumColumn($conn,$column,$table){
	$sql = "SELECT SUM(".$column.") FROM ".$table."";
	$iteminfo = mysqli_query($conn,$sql);
	mysqliDie($iteminfo);
	$iteminforow = mysqli_fetch_array($iteminfo);
	return $iteminforow['SUM('.$column.')'];
}

//sum selected column from db
function sumSpecificColumn($conn,$column,$table,$where,$rowid){
	$sql = "SELECT SUM(".$column.") FROM ".$table." WHERE ".$where."='".$rowid."'";
	$iteminfo = mysqli_query($conn,$sql);
	mysqliDie($iteminfo);
	$iteminforow = mysqli_fetch_array($iteminfo);
	return $iteminforow['SUM('.$column.')'];
}

//sum selected column from db
function customQuery($conn,$sql,$column){
	$sql = $sql;
	$iteminfo = mysqli_query($conn,$sql);
	mysqliDie($iteminfo);
	$iteminforow = mysqli_fetch_array($iteminfo);
	return $iteminforow[$column];
}

//store activity logs to database
function addLog($conn,$username,$activity){
	$log_time = time();
	$log_date = date('m/d/Y',time());
	$log_day = date('j',time());
	$log_month = date('n',time());
	$log_year = date('Y',time());
	$sql = "INSERT INTO logs (username, activity, log_date, log_day, log_month, log_year, log_time)".
	" VALUES ('".$username."','".$activity."','".$log_date."','".$log_day."','".$log_month."','".$log_year."','".$log_time."')";
	$results = mysqli_query($conn,$sql);
	mysqliDie($results);
}

//check user title
function userTitle($value){
	if($value == 1){
		return 'Mr.';
	}else{
		return 'Miss';
	}
}

//check gender
function userGender($value){
	if($value == 1){
		return 'Male';
	}else{
		return 'Female';
	}
}

//check marital status
function maritalStatus($value){
	if($value == 1){
		echo 'Single';
	}else{
		echo 'Married';
	}
}

//check if form item is checked
function isChecked($key,$value){
	if($key == $value){
		return 'checked';
	}else{
		return '';
	}
}

//check permissions checkboxes
function checkPermission($key,$array){
	$value = explode(',',$array);
	if(in_array($key, $value)){
		return 'checked';
	}else{
		return '';
	}
}

//Get full month name
function getPeriod($period){
	$date = strtotime(str_replace('/', '-', $period));
	return date('M Y',$date);
}

//member status
function memberStatus($value){
	if($value == 0){
		return '<span class="label label-warning">Archived</span>';
	}elseif($value == 1){
		return '<span class="label label-success">Active</span>';
	}else{
		return '<span class="label label-danger">Error</span>';
	}
}

//loan status
function loanStatus($value){
	if($value < 0){
		return '<span class=\"label label-warning\">Pending</span>';
	}elseif($value >= 0){
		return '<span class=\"label label-success\">Paid</span>';
	}else{
		return '<span class=\"label label-danger\">Error</span>';
	}
}

//asset status
function assetStatus($value){
	if($value == 1){
		return '<span class=\"label label-success\">In-Use</span>';
	}elseif($value == 2){
		return '<span class=\"label label-danger\">Faulty</span>';
	}elseif($value == 3){
		return '<span class=\"label label-warning\">Not In-Use</span>';
	}
}

//get the total payment duration from total numeber of months
function paymentDuration($months){
	$months = floor($months);
	$year = floor($months/12);
	$year_more = $year * 12;
	$new_month = abs($year_more - $months);
	//conditions
	switch($months){
		case($months == 1):
		return $months." Month";
		break;

		case($months > 1 && $months < 12):
		return $months." Months";
		break;

		case($months == 12):
		return  $year." Year";
		break;

		case($months > 12):
		return ($year > 1 ? $year.' Years ' : $year.' Year ').
			 ($new_month == 0 ? '' : ($new_month > 1 ? $new_month.' Months ' : $new_month.' Month '));
		break;
	}
}

//set notification type
function setNotification($type,$prefix='',$message,$postfix='',$referer=''){
	setcookie('notification_type', $type, time() + (60), '/');
	setcookie('notification', $prefix.$message.$postfix, time() + (60), '/');
	($referer == '') ? $ref = $_SERVER['HTTP_REFERER'] : $ref = $referer;
	header('Location:'.$ref);
}

//show notification
function showNotification(){
	if(isset($_COOKIE['notification_type']) && $_COOKIE['notification_type'] == 1){

		setcookie('notification_type', '1', time() - (60), '/');
		return '<div class="alert alert-success" role="alert"><button class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> '.$_COOKIE['notification'].'</div>';

	}elseif(isset($_COOKIE['notification_type']) && $_COOKIE['notification_type'] == 2){

		setcookie('notification_type', '2', time() - (60), '/');
		return '<div class="alert alert-danger" role="alert"><button class="close" data-dismiss="alert">×</button>
        <strong>Error!</strong> '.$_COOKIE['notification'].'</div>';

	}elseif(isset($_COOKIE['notification_type']) && $_COOKIE['notification_type'] == 3){

		setcookie('notification_type', '3', time() - (60), '/');
		return '<div class="alert alert-info" role="alert"><button class="close" data-dismiss="alert">×</button>
        <strong>Info!</strong> '.$_COOKIE['notification'].'</div>';

	}elseif(isset($_COOKIE['notification_type']) && $_COOKIE['notification_type'] == 4){

		setcookie('notification_type', '4', time() - (60), '/');
		return '<div class="alert alert-warning" role="alert"><button class="close" data-dismiss="alert">×</button>
        <strong>Warning!</strong> '.$_COOKIE['notification'].'</div>';

	}else{

		return '';

	}
}

//check the current user role
function userRole($value){
	if($value == 1){
		return 'Administrator';
	}else{
		return 'Standard';
	}
}

//Get full month name
function dateFormat($str_date,$format,$type = 0){

	if($type == 1){
		$date = strtotime(str_replace('/', '-', $str_date));
	}else{
		$date = strtotime($str_date);
	}

	//conditions
	switch($format){
		case($format == 'DD/MM/YYYY'):
		return date('d/m/Y',$date);
		break;

		case($format == 'MM/DD/YYYY'):
		return date('m/d/Y',$date);
		break;

		case($format == 'DD-MM-YYYY'):
		return date('d-m-Y',$date);
		break;

		case($format == 'DD-MMM-YYYY'):
		return date('d-M-Y',$date);
		break;

		case($format == 'MM-DD-YYYY'):
		return date('m-d-Y',$date);
		break;

		case($format == 'MMMM YYYY'):
		return date('F Y',$date);
		break;
	}
}

//calculate the age from the date of birth
function getAge($date){
	if($date != ''){
		$age = substr($date,-4,4);
		$current_date = date('Y',time());
		return ($current_date - $age);
	}else{
		return '<em>None</em>';
	}
}

//count all rows from table
function countAll($conn,$table){
	$sql = "SELECT * FROM ".$table."";
	$iteminfo = mysqli_query($conn,$sql);
	mysqliDie($iteminfo);
	$iteminfocount = mysqli_num_rows($iteminfo);
	return $iteminfocount;
}

//last seen time function
function LastSeen($lasttime){
	$date1 = $lasttime; //we use time() to store timestamp
	$date2 = time();
	$dateDiff = $date2 - $date1;
	$fullSeconds = floor($dateDiff / 1);
	$fullMinutes = floor($dateDiff / 60);
	$fullHours = floor($dateDiff / (60*60));
	$fullDays = floor($dateDiff / (60*60*24));
	$fullMonths = floor($dateDiff / (60*60*24*30));
	$fullYears = floor($dateDiff / (60*60*24*30*12));

	//conditions
	switch($date = "2011")
		{
		case($dateDiff > 0 && $dateDiff < 60):
		return "$fullSeconds second".($fullSeconds > 1 ? 's' : '')." ago";
		break;

		case($fullSeconds > 59 && $fullSeconds < 3601):
		return "$fullMinutes minute".($fullMinutes > 1 ? 's' : '')." ago";
		break;

		case($fullSeconds > 3600 && $fullSeconds < 86401):
		return "$fullHours hour".($fullHours > 1 ? 's' : '')." ago";
		break;

		case($fullSeconds > 86400 && $fullSeconds < 2592001):
		return "$fullDays day".($fullDays > 1 ? 's' : '')." ago";
		break;

		case($fullSeconds > 2592001 && $fullSeconds < 31104001):
		return "$fullMonths month".($fullMonths > 1 ? 's' : '')." ago";
		break;

		case($fullSeconds > 31104000):
		return "$fullYears year".($fullYears > 1 ? 's' : '')." ago";
		break;
		}
}


?>
