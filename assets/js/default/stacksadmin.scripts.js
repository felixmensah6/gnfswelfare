/*!
 * Stacks Admin Panel Project
 * Copyright 2015, Stacks Ghana.
 * www.stacksgh.com
 */
 
// Start Document Ready
$(document).ready(function(){

/*************************************** 
 GLOBALLY DEFINED VARIABLES
***************************************/

	/* Window Height */
	var winHeight = $(window).height();
	
	/* Window Width */
	var winWidth = $(window).width();
	
		 
/*************************************** 
 ACCORDION MENU
***************************************/
	$(".sidebar-navbar,.sidebar-mini-navbar").accordion({
		accordion: true,
		speed: 400,
		closedSign: '<span class="icon-angle-right"></span>',
		openedSign: '<span class="icon-angle-down"></span>'
	});
	
	
/*************************************** 
 TOGGLE SIDEBAR ON MOBILE DEVICES
***************************************/
	$(".toggle-mobile").click(function () {
		var mobile_sidebar = $('.sidebar');
		if(mobile_sidebar.hasClass("slideInLeft")){
			mobile_sidebar.removeClass("slideInLeft").addClass("slideOutLeft");
			setTimeout(function(){ 
				mobile_sidebar.removeClass("dp-bk").removeClass("slideOutLeft"); 
			}, 500);
		}else{
			mobile_sidebar.removeClass("slideOutLeft").addClass("slideInLeft").addClass("dp-bk");
		}
		return false;
	});
	
	
/*************************************** 
 TOGGLE SIDEBAR ON LARGE DEVICES
***************************************/
			
	$(".toggle-sidebar").click(function () {
		var SidebarNavbar = $(".sidebar-navbar"),
			SidebarNavMini = $('.sidebar-mini-navbar'), 
			MainSidebar = $('.sidebar'),
			MainContent = $('.content');
		if(SidebarNavbar.hasClass("sidebar-navbar")){
			SidebarNavbar.addClass("sidebar-mini-navbar").removeClass("sidebar-navbar");
			MainSidebar.addClass("sidebar-mini").removeClass("ps-fx");
			MainContent.addClass("content-maxi");
		}else{
			SidebarNavMini.addClass("sidebar-navbar").removeClass("sidebar-mini-navbar");
			MainSidebar.removeClass("sidebar-mini");
			MainContent.removeClass("content-maxi");
			if($('.sidebar-nav').hasClass("sidebar-fixed-nav")){
				MainSidebar.addClass("ps-fx");
			}
		}
		return false;
	}); 
	
	
	$(window).mutate('width',function (el,info){
		/* Reset Sidebar on Mobile Devices */
		if($(el).width() <= 768){
			$('.sidebar-mini-navbar').addClass("sidebar-navbar").removeClass("sidebar-mini-navbar");
			$('.sidebar').removeClass("sidebar-mini");
			$('.content').removeClass("content-maxi");
		}
		
		/* Remove Fixed Position if Sidebar is Minimized on Resize */
		if($(el).width() <= 1024){
			//$('.sidebar-nav').removeClass("sidebar-fixed-nav");
			$('.sidebar').removeClass("ps-fx");
		}
		
		if($(el).width() > 1024){
			if($('.sidebar-nav').hasClass("sidebar-fixed-nav")){
				//$('.sidebar-nav').addClass("sidebar-fixed-nav");
				$('.sidebar').addClass("ps-fx");
			}
		}
	});
	
	/* Remove Fixed Position if Sidebar is Minimized on Load */
	if($(window).width() <= 1024){
		$('.sidebar-nav').removeClass("sidebar-fixed-nav");
		$('.sidebar').removeClass("ps-fx");
	}
	
	
/*************************************** 
 TOGGLE QUICK TOOLBAR
***************************************/
	$(".toggle-quick-toolbar").click(function () {
		var quick_toolbar = $('.quick-toolbar');
		if(quick_toolbar.is( ":visible" ) ){
			quick_toolbar.slideUp();
		}else{
			quick_toolbar.slideDown();
		}
		return false;
	});


/*************************************** 
 TOGGLE SEARCH BAR
***************************************/
	$(".toggle-search").click(function () {
		$('.toggle-search-box').addClass("zoomIn").removeClass("zoomOut").addClass("dp-bk");
		return false;
	});
	
	$(".toggle-search-close").click(function () {
		$('.toggle-search-box').addClass("zoomOut").removeClass("zoomIn");
		setTimeout(function(){ 
			$('.toggle-search-box').removeClass("dp-bk").removeClass("zoomOut");
		}, 800);
		return false;
	});


/*************************************** 
 CUSTOM SCROLLBAR
***************************************/
	
	/* Sidebar Scrollbar */
	$(".sidebar-fixed-nav, .sidebar-nav").mCustomScrollbar({
		autoHideScrollbar:true,
	});
	
	/* Panel Scrollbar */
	$(".panel-collapse").mCustomScrollbar({
		autoHideScrollbar:true,
		theme:"minimal-dark",
	});
	
	
/*************************************** 
 PANEL COLLAPSE
***************************************/
	$(".toggle-panel").click(function() {
		$(this).closest('.panel').find('.panel-collapse').slideToggle(300, function() {
		  var ChangeIcon = $(this).closest('.panel').find('.toggle-panel i');
		  if(ChangeIcon.hasClass("icon-minus")){
			  ChangeIcon.removeClass("icon-minus").addClass("icon-plus");
		  }else{
			  ChangeIcon.removeClass("icon-plus").addClass("icon-minus");
		  }
		});
		return false;
	});
	
	
/*************************************** 
 TOGGLE PANEL FULLSCREEN
***************************************/
	$(".panel").on('click','.panel-screen-on',function () {
		var panelExpandIcon = $(this).closest('.panel').find('.panel-screen-on i');
		var panelCollapseDiv = $(this).closest('.panel').find('.panel-collapse');
		$(this).closest('.panel').addClass("animated").addClass("zoomIn")
		.addClass("panel-screen").removeClass("zoomOut");
		$(this).removeClass("panel-screen-on").addClass("panel-screen-off");
		panelCollapseDiv.addClass("panel-collapse-scroll");
		$('body').addClass("ov-h");
		$('.header').addClass("z-1");
		panelExpandIcon.removeClass("icon-expand").addClass("icon-compress");
		return false;
	});

	$(".panel").on('click', ".panel-screen-off", function() {
		var panelCompressIcon = $(this).closest('.panel').find('.panel-screen-off i');
		var panelCollapseDiv = $(this).closest('.panel').find('.panel-collapse');
		$(this).closest('.panel').removeClass("zoomIn").removeClass("panel-screen").removeClass("animated");
		$(this).addClass("panel-screen-on").removeClass("panel-screen-off");
		panelCollapseDiv.removeClass("panel-collapse-scroll");
		$('body').removeClass("ov-h");
		$('.header').removeClass("z-1");
		panelCompressIcon.removeClass("icon-compress").addClass("icon-expand");
		return false;        
	});


/*************************************** 
 FIX BOOTSTRAP MODAL SHIFTING HEADER
***************************************/
	function getScrollBarWidth(){
	    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
	        widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
	    $outer.remove();
	    var newWidth = 100 - widthWithScroll;
	    if(newWidth === 0){
	    	return 0;
	    }else{
	    	return newWidth;
	    }
	};

    $('.modal').on('show.bs.modal', function () {
        $('.header').css('padding-right', getScrollBarWidth() + 'px');
    })

    $('.modal').on('hidden.bs.modal', function () {
        $('.header').css('padding-right', '0px');
    })


});
// End Document Ready


/*************************************** 
 FULLSCREEN WINDOW
***************************************/
	
	/* Start Fullscreen Function */
	function startFullscreen(element) {
		if(element.requestFullscreen) {
			element.requestFullscreen();
		} else if(element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if(element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} else if(element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}
	}
	
	/* Exit Fullscreen Function */
	function exitFullscreen() {
		if(document.exitFullscreen) {
			document.exitFullscreen();
		} else if(document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if(document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}
	
	/* Toggle Fullscreen */
	$(".toggle-fullscreen-on").click(function () {
		var ToggleFullscreen = $('.toggle-fullscreen-on');
		if(ToggleFullscreen.hasClass("toggle-fullscreen-on")){
			ToggleFullscreen.addClass("toggle-fullscreen-off").removeClass("toggle-fullscreen-on");
			startFullscreen(document.documentElement);
		}else{
			$('.toggle-fullscreen-off').addClass("toggle-fullscreen-on").removeClass("toggle-fullscreen-off");
			exitFullscreen();
		}
		return false;
	});
	
	


