/*!
 * Stacks Admin Panel Project
 * Copyright 2015, Stacks Ghana.
 * www.stacksgh.com
 */
 
/*************************************** 
 ACCORDION MENU
***************************************/
(function($){
    $.fn.extend({
    //pass the options variable to the function
    accordion: function(options) {      
		var defaults = {
			accordion: 'true',
			speed: 300,
			closedSign: '[+]',
			openedSign: '[-]'
		};
		// Extend our default options with those provided.
		var opts = $.extend(defaults, options);
		//Assign current element to variable, in this case is UL element
 		var $this = $(this); 		
 		//add a mark [+] to a multilevel menu
 		$this.find("li").each(function() {
 			if($(this).find("ul").size() != 0){
 				//add the multilevel sign next to the link
 				$(this).find("a:first").append("<span class='iconic'>"+ opts.closedSign +"</span>"); 				
 				//avoid jumping to the top of the page when the href is an #
 				if($(this).find("a:first").attr('href') == "#"){
 		  			$(this).find("a:first").click(function(){return false;});
 		  		}
 			}
 		});
 		//open active level
 		$this.find("li a.active").each(function() {
 			$(this).parents("ul").slideDown(opts.speed);
 			$(this).parents("ul").parent("li").find("span.iconic:first").html(opts.openedSign);
 		});
  		$this.find("li a").click(function() {
  			if($(this).parent().find("ul").size() != 0){
  				if(opts.accordion){
  					//Do nothing when the list is open
  					if(!$(this).parent().find("ul").is(':visible')){
  						parents = $(this).parent().parents("ul");
  						visible = $this.find("ul:visible");
  						visible.each(function(visibleIndex){
  							var close = true;
  							parents.each(function(parentIndex){
  								if(parents[parentIndex] == visible[visibleIndex]){
  									close = false;
  									return false;
  								}
  							});
  							if(close){
  								if($(this).parent().find("ul") != visible[visibleIndex]){
  									$(visible[visibleIndex]).slideUp(opts.speed, function(){
  										$(this).parent("li").find("span.iconic:first").html(opts.closedSign);
  									});
  									
  								}
  							}
  						});
  					}
  				}
  				if($(this).parent().find("ul:first").is(":visible")){
  					$(this).parent().find("ul:first").slideUp(opts.speed, function(){
  						$(this).parent("li").find("span.iconic:first").delay(opts.speed).html(opts.closedSign);
  					}); 					
  				}else{
  					$(this).parent().find("ul:first").slideDown(opts.speed, function(){
  						$(this).parent("li").find("span.iconic:first").delay(opts.speed).html(opts.openedSign);
  					});
  				}
  			}
  		});
    }
});
})(jQuery);


/*************************************** 
 JQUERY MUTATE PLUGIN
***************************************/
!function(a){mutate_event_stack=[{name:"width",handler:function(b){return n={el:b},a(n.el).data("mutate-width")||a(n.el).data("mutate-width",a(n.el).width()),a(n.el).data("mutate-width")&&a(n.el).width()!=a(n.el).data("mutate-width")?(a(n.el).data("mutate-width",a(n.el).width()),!0):!1}},{name:"height",handler:function(b){return element=b,a(element).data("mutate-height")||a(element).data("mutate-height",a(element).height()),a(element).data("mutate-height")&&a(element).height()!=a(element).data("mutate-height")?(a(element).data("mutate-height",a(element).height()),!0):void 0}},{name:"top",handler:function(b){return a(b).data("mutate-top")||a(b).data("mutate-top",a(b).css("top")),a(b).data("mutate-top")&&a(b).css("top")!=a(b).data("mutate-top")?(a(b).data("mutate-top",a(b).css("top")),!0):void 0}},{name:"bottom",handler:function(b){return a(b).data("mutate-bottom")||a(b).data("mutate-bottom",a(b).css("bottom")),a(b).data("mutate-bottom")&&a(b).css("bottom")!=a(b).data("mutate-bottom")?(a(b).data("mutate-bottom",a(b).css("bottom")),!0):void 0}},{name:"right",handler:function(b){return a(b).data("mutate-right")||a(b).data("mutate-right",a(b).css("right")),a(b).data("mutate-right")&&a(b).css("right")!=a(b).data("mutate-right")?(a(b).data("mutate-right",a(b).css("right")),!0):void 0}},{name:"left",handler:function(b){return a(b).data("mutate-left")||a(b).data("mutate-left",a(b).css("left")),a(b).data("mutate-left")&&a(b).css("left")!=a(b).data("mutate-left")?(a(b).data("mutate-left",a(b).css("left")),!0):void 0}},{name:"hide",handler:function(b){return a(b).is(":hidden")?!0:void 0}},{name:"show",handler:function(b){return a(b).is(":visible")?!0:void 0}},{name:"scrollHeight",handler:function(b){return a(b).data("prev-scrollHeight")||a(b).data("prev-scrollHeight",a(b)[0].scrollHeight),a(b).data("prev-scrollHeight")&&a(b)[0].scrollHeight!=a(b).data("prev-scrollHeight")?(a(b).data("prev-scrollHeight",a(b)[0].scrollHeight),!0):void 0}},{name:"scrollWidth",handler:function(b){return a(b).data("prev-scrollWidth")||a(b).data("prev-scrollWidth",a(b)[0].scrollWidth),a(b).data("prev-scrollWidth")&&a(b)[0].scrollWidth!=a(b).data("prev-scrollWidth")?(a(b).data("prev-scrollWidth",a(b)[0].scrollWidth),!0):void 0}},{name:"scrollTop",handler:function(b){return a(b).data("prev-scrollTop")||a(b).data("prev-scrollTop",a(b)[0].scrollTop()),a(b).data("prev-scrollTop")&&a(b)[0].scrollTop()!=a(b).data("prev-scrollTop")?(a(b).data("prev-scrollTop",a(b)[0].scrollTop()),!0):void 0}},{name:"scrollLeft",handler:function(b){return a(b).data("prev-scrollLeft")||a(b).data("prev-scrollLeft",a(b)[0].scrollLeft()),a(b).data("prev-scrollLeft")&&a(b)[0].scrollLeft()!=a(b).data("prev-scrollLeft")?(a(b).data("prev-scrollLeft",a(b)[0].scrollLeft()),!0):void 0}}]}(jQuery);;(function($){mutate={speed:1,event_stack:mutate_event_stack,stack:[],events:{},add_event:function(evt){mutate.events[evt.name]=evt.handler;},add:function(event_name,selector,callback,false_callback){mutate.stack[mutate.stack.length]={event_name:event_name,selector:selector,callback:callback,false_callback:false_callback}}};function reset(){var parent=mutate;if(parent.event_stack!='undefined'&&parent.event_stack.length){$.each(parent.event_stack,function(j,k){mutate.add_event(k);});}
parent.event_stack=[];$.each(parent.stack,function(i,n){$(n.selector).each(function(a,b){if(parent.events[n.event_name](b)===true){if(n['callback'])n.callback(b,n);}else{if(n['false_callback'])n.false_callback(b,n)}})})
setTimeout(reset,mutate.speed);}
reset();$.fn.extend({mutate:function(){var event_name=false,callback=arguments[1],selector=this,false_callback=arguments[2]?arguments[2]:function(){};if(arguments[0].toLowerCase()=='extend'){mutate.add_event(callback);return this;}
$.each($.trim(arguments[0]).split(' '),function(i,n){event_name=n;mutate.add(event_name,selector,callback,false_callback);});return this;}});})(jQuery);


/*************************************** 
 CHECK ALL CHECKBOXES PLUGIN
***************************************/
(function($) {
  $.fn.tCheckAll = function(options) 
  { 
    return this.each(function(index, element) {
      var base = element,
      $base    = $(element);

      var condition = true;
      var name      = $("#"+ $base.attr('id'));
      
      base.init = function() {
        name.on("click", function(event)
        {
          event.preventDefault();
          var control = $(this);
          $(this).closest(options).find(':checkbox').each(function(){  
            if( condition )
              $(this).prop('checked', true);
            else
              $(this).prop('checked', false);
          });
          
          if( condition )
            base.checkAll(control);
          else
            base.unCheckAll(control);
          condition = !condition;
        });
      };
    
      base.checkAll = function(control) {
        control.trigger("beforeCheck");
      };
    
      base.unCheckAll = function(control) {
        control.trigger("beforeUnCheck");
      };
    
      base.init();    
    });
  };
})(jQuery);