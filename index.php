<?php
//start output buffering
ob_start();

//include the configuration and functions
require_once('module/include/session.php');
require_once('module/include/dbconnect.php');
require_once('module/include/class.php');
require_once('module/include/config.php');
require_once('module/include/function.php');
require_once('module/include/pages.php');


//check if the user is logged in then redirect
if(isset($_SESSION['user_id'])){ 

//include the header
require(SYS_PATH . 'header.php');

//fetch pages
fetchPages($getPages,DEFAULT_PAGE);

//include the footer		
require(SYS_PATH . 'footer.php');

}else {
	require(SYS_PATH . 'login.php');
}


//flush output buffering
ob_end_flush(); 
?>